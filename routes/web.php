<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/farm-upsystem', 'HomeController@farmupsystem');

Route::get('/farm-upsystem', function () {
    header("Location: https://taitanfarm.com/public/files/farm-upsystem.apk");
die();
});

Route::get('/', 'HomeController@home');
Route::get('/signin', 'HomeController@signin');
Route::post('dosignin', 'HomeController@dosignin');
Route::get('login', [ 'as' => 'login', 'uses' => 'HomeController@signin']);
Route::get('/register', 'HomeController@register');
Route::post('doRegister', 'HomeController@doRegister');
Route::get('/logout', 'HomeController@logout');
Route::get('/activateAccount', 'HomeController@activateAccount');

Route::get('ResetPasswordLink/{PasswordConfirmation}/{email}', 'ForgotpasswordController@resetPasswordLink');
Route::get('forgotPassword', 'ForgotpasswordController@forgotPassword');
Route::post('resetPassword', 'ForgotpasswordController@resetPassword');
Route::post('DoResetMYPassword', 'ForgotpasswordController@doResetPassword');

Route::get('resendRecoveryEmail/{email}', 'ForgotpasswordController@resendRecoveryEmail');

Route::get('subscribe', 'HomeController@subscribe')->name('subscribe');
Route::get('requestquote', 'HomeController@requestquote')->name('requestquote');

Route::post('/request_avocado_farming_quotation', 'SiteController@request_avocado_farming_quotation');

Route::get('/help', 'SiteController@help');

Route::get('/knowledge_centre', 'KnowledgecentreController@knowledge_centre');

Route::get('/knowledgecentre/{slug}', 'KnowledgecentreController@knowledgecentre');

Route::get('/agri_financing', 'AgrifinancingController@agri_financing');

Route::get('/about_us', function () {
    return view('about_us');
});

Route::get('/water_harvesting_irrigation', function () {
    return view('water_harvesting_irrigation');
});

Route::get('/products_services', function () {
    return view('products_services');
});

Route::get('/avocado_farming', function () {
    return view('avocado_farming');
});

Route::get('/dairy_farm', function () {
    return view('dairy_farm');
});

Route::get('/bulls_meat_taitan_flavours', function () {
    return view('bulls_meat_taitan_flavours');
});

Route::get('/taitan_nurseries', function () {
    return view('taitan_nurseries');
});

Route::get('/taitan_kennels', function () {
    return view('taitan_kennels');
});

Route::get('/water_irrigation', function () {
    return view('water_irrigation');
});

Route::get('/help_me_start_my_farm', function () {
    return view('help_me_start_my_farm');
});

Route::get('/privacy', function () {
    return view('privacy');
});

Route::get('/terms', function () {
    return view('terms');
});
