-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2023 at 07:27 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taitanfa_farm`
--

-- --------------------------------------------------------

--
-- Table structure for table `agri_financing`
--

CREATE TABLE `agri_financing` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `link` text DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `pdf` varchar(50) DEFAULT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `agri_financing`
--

INSERT INTO `agri_financing` (`id`, `title`, `slug`, `link`, `photo`, `pdf`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Test title link', 'test-title', 'https://stackoverflow.com/questions/13955667/disab', NULL, NULL, 0, '2023-05-04 15:44:08', '2023-05-04 15:44:08'),
(2, 'Test pdf', 'test-pdf', NULL, NULL, 'pdf.pdf', 0, '2023-05-04 15:47:46', '2023-05-04 15:47:46'),
(3, 'Test photo', 'test-photo', NULL, 'photo.jpg', NULL, 0, '2023-05-04 15:47:46', '2023-05-04 15:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `email_subscriptions`
--

CREATE TABLE `email_subscriptions` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_subscriptions`
--

INSERT INTO `email_subscriptions` (`id`, `email`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'jjhjhj@hghg.hjhjh', 0, '2020-08-03 05:34:47', '2020-10-22 08:21:47'),
(2, 'brrrrrrrrrr@gggg.bb', 0, '2020-08-03 05:47:01', '2020-10-22 08:21:47'),
(3, 'b@ghjhgjhg.hjhgjhg', 0, '2020-08-24 15:07:54', '2020-10-22 08:21:47'),
(4, 'vbcmcvmbvc@dsfsf.sdfsd', 0, '2020-11-21 09:50:10', '2020-11-21 09:50:10'),
(5, 'medevkenya@gmail.com', 0, '2020-11-23 09:46:44', '2020-11-23 09:46:44'),
(6, 'mervecabs@gmail.com', 0, '2020-11-23 09:47:43', '2020-11-23 09:47:43'),
(7, 'brian.mulunda@ajiraconnect.com', 0, '2020-11-23 09:49:06', '2020-11-23 09:49:06'),
(8, 'brianmfulu4@yahoo.com', 0, '2020-11-23 09:49:59', '2020-11-23 09:49:59'),
(9, 'medevkenyaeewe@gmail.com', 0, '2021-03-24 14:37:33', '2021-03-24 14:37:33'),
(10, 'globaltempingservices@gmail.com', 0, '2021-03-24 14:42:26', '2021-03-24 14:42:26');

-- --------------------------------------------------------

--
-- Table structure for table `knowledge_centre`
--

CREATE TABLE `knowledge_centre` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `photo` varchar(50) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT 1,
  `slug` text NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `knowledge_centre`
--

INSERT INTO `knowledge_centre` (`id`, `title`, `description`, `photo`, `status`, `slug`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'White Gold Milk\r\nProduction', 'Youghurt, Milk and ice cream processing wing is located at the farm where the Farm purchases milk from other farmers to produce the famous white gold yoghurt.', 'photo.jpg', 1, 'white1', 0, '2023-05-04 09:20:47', '2023-05-04 10:45:47'),
(2, 'White Gold Milk\r\nProduction', 'Youghurt, Milk and ice cream processing wing is located at the farm where the Farm purchases milk from other farmers to produce the famous white gold yoghurt.', 'photo.jpg', 1, 'white2', 0, '2023-05-04 10:28:59', '2023-05-04 10:45:51'),
(3, 'White Gold Milk\r\nProduction', 'Youghurt, Milk and ice cream processing wing is located at the farm where the Farm purchases milk from other farmers to produce the famous white gold yoghurt.', 'photo.jpg', 1, 'white3', 0, '2023-05-04 10:30:19', '2023-05-04 10:45:55'),
(4, 'White Gold Milk\r\nProduction', 'Youghurt, Milk and ice cream processing wing is located at the farm where the Farm purchases milk from other farmers to produce the famous white gold yoghurt.', 'photo.jpg', 1, 'white4', 0, '2023-05-04 10:30:21', '2023-05-04 10:46:00');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` int(11) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `mobileNo` varchar(20) NOT NULL,
  `service` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `firstName`, `lastName`, `mobileNo`, `service`, `message`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'fsdf', 'sdfsdf', '4543645654', 'Pigs', 'dfgdfgdbbfdgdf', 0, '2021-05-20 02:15:58', '2021-05-20 02:15:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agri_financing`
--
ALTER TABLE `agri_financing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_subscriptions`
--
ALTER TABLE `email_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `knowledge_centre`
--
ALTER TABLE `knowledge_centre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agri_financing`
--
ALTER TABLE `agri_financing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `email_subscriptions`
--
ALTER TABLE `email_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `knowledge_centre`
--
ALTER TABLE `knowledge_centre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
