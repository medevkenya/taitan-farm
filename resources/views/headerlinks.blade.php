<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Covered+By+Your+Grace&amp;display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('css/animate.css%2bowl.carousel.min.css%2bowl.theme.default.min.css%2bmagnific-popup.css%2bflaticon.css%2bstyle.css.pagespeed.cc.EpFxHEog3Y.css')}}" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="icon" type="image/png" href="{{asset('images/favicon-32x32.png')}}">
<link rel="apple-touch-icon" type="image/png" href="{{asset('images/apple-touch-icon.png')}}">
