<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image:url(images/xbg_3.jpg.pagespeed.ic.EtzwAFKmq2.jpg)">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span class="mr-2"><a href="{{URL::to('/pigfarm')}}">Poultry Farm
   <i class="fa fa-chevron-right"></i></a></span>

   </p>
<h1 class="mb-0 bread">Poultry Farm</h1>
</div>
</div>
</div>
</section>
<section class="ftco-section">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">
<h2 class="mb-3">Eggcellent Eggs Company</h2>
<p>The farm has over 4,000-layer birds that produce eggs that are packaged under
eggcellent brand.</p>
<p>
<img src="images/work-5.jpg" alt="" class="img-fluid">
</p>
<p>The poultry section composes of Kienyeji (Indigenous) chicken breed and
improved breeds. The farm has a 520 egg hatchery that produces chicks for sale. The breeds found are Kenbro, Kienyeji, Raibow rosters, Kuroilers, Geese, Turkeys, Pekins and Ducks</p>

</div>
@include('sidebar')
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
