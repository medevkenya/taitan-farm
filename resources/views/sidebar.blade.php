<div class="col-lg-4 sidebar ftco-animate pl-md-4">
<!-- <div class="sidebar-box bg-light rounded">
<form action="#" class="search-form">
<div class="form-group">
<span class="icon fa fa-search"></span>
<input type="text" class="form-control" placeholder="Search...">
</div>
</form>
</div> -->
<div class="sidebar-box ftco-animate">
<div class="categories">
<h3>Our services</h3>
<li><a href="{{URL::to('/help_me_start_my_farm')}}">Consultancy and farm set up</a></li>
<li><a href="{{URL::to('/avocado_farming')}}">Agronomy services</a></li>
<li><a href="{{URL::to('/help_me_start_my_farm')}}">Water harvesting, irrigation and drilling solutions</a></li>
<li><a href="{{URL::to('/agri_financing')}}">Agribusiness market opportunities</a></li>
<li><a href="{{URL::to('/agri_financing')}}">Agribusiness Financing opportunities</a></li>
<li><a href="{{URL::to('/help_me_start_my_farm')}}">Soil tests and analysis</a></li>
<li><a href="{{URL::to('/dairy_farm')}}">Dairy farming training and construction</a></li>
</div>
</div>

<div class="sidebar-box ftco-animate">
<div class="categories">
<h3>Our products</h3>
<li><a href="{{URL::to('/dairy_farm')}}">Quality dairy animals</a></li>
<li><a href="{{URL::to('/avocado_farming')}}">Avocado farm set up & orchards</a></li>
<li><a href="{{URL::to('/avocado_farming')}}">Avocado seedlings & farm management</a></li>
<li><a href="{{URL::to('/avocado_farming')}}">Avocado exporting</a></li>
<li><a href="{{URL::to('/taitan_kennels')}}">German shepherd puppies</a></li>
<li><a href="{{URL::to('/help_me_start_my_farm')}}">Apiaries and beehives</a></li>
<li><a href="{{URL::to('/help_me_start_my_farm')}}">Bull fattening, Poultry & Pork</a></li>
<li><a href="{{URL::to('/products_services')}}">Processing of milk, avocado oil & honey</a></li>
<li><a href="{{URL::to('/dairy_farm')}}">Dairy inputs like fodder and silage delivered to you</a></li>
</div>
</div>

</div>
