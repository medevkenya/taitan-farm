<!DOCTYPE html>
<html lang="en">
<head>
<title>Taitan Nurseries | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/TaitanNurseries/banner.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Taitan Nurseries <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Taitan Nurseries</h1>
</div>
</div>
</div>
</section>


<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<!-- <h2 class="mb-3">Taitan Nurseries</h2> -->
<p>We are not in the business of selling seedlings but target buying Hass Avocado fruits from farmers
	for export, since of many of fake varieties of roadside seedlings we decided to have a say on the
	quality of seedlings our farmers get as well us offer farm management support.
	The farm holds Avocado nurseries in four locations in Kenya. At head office in Taita Taveta, Wundanyi;
	Makuyu in Muranga county. Loitoktok and Kajiado.</p>
	<p>We only sell seedlings that we have visibility of a steady market with and we have also partnered with other farms to offer what is beneficially to farmers but we have no specialty in.
		<a href="{{URL::to('/knowledge_centre')}}">Learn more on our knowledge center on why Hass avocado</a></p>

<h3>Order your seedlings now</h3>

{!! Form::open(['url' => 'request_taitan_nurseries_quotation']) !!}
<div class="row">
<div class="col-md-8 form-group">
<label>Your Name</label>
<input class="form-control" type="text" name="name" value="{{old('name')}}" required>
@if ($errors->has('name'))
   <span class="text-danger">{{ $errors->first('name') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Your Mobile No.</label>
<input class="form-control" type="text" name="mobileNo" value="{{old('mobileNo')}}" required>
@if ($errors->has('mobileNo'))
   <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
@endif
</div>
<div class="col-md-8 form-group">
<label>Your Location</label>
<input class="form-control" type="text" name="location" value="{{old('location')}}" required>
@if ($errors->has('location'))
   <span class="text-danger">{{ $errors->first('location') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Quantity</label>
<input class="form-control" type="text" name="quantity" value="{{old('quantity')}}" required>
@if ($errors->has('quantity'))
   <span class="text-danger">{{ $errors->first('quantity') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
<label>Details about your order (Type of seedlings)</label>
<textarea class="form-control" name="description" required>{{old('description')}}</textarea>
@if ($errors->has('description'))
   <span class="text-danger">{{ $errors->first('description') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
	<button type="submit" class="btn btn-primary">Send Request</button>
</div>
</div>
</form>

</div>

@include('sidebar')

<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:10%;">
<div class="container">
<div class="row">
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/TaitanNurseries/IMG-20230429-WA0012.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/TaitanNurseries/IMG-20230429-WA0019.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/TaitanNurseries/IMG-20230429-WA0042.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/TaitanNurseries/IMG-20230429-WA0046.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/TaitanNurseries/IMG-20230429-WA0090.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/TaitanNurseries/IMG-20230429-WA0101.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/TaitanNurseries/IMG-20230429-WA0102.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/TaitanNurseries/IMG-20230429-WA0108.jpg')}});">
</div>
</div>
</div>
</div>
</div>
</section>

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
