<!DOCTYPE html>
<html lang="en">
<head>
<title>White Gold | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>White Gold <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">White Gold</h1>
</div>
</div>
</div>
</section>

<section class="ftco-section">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">
<h2 class="mb-3">White Gold</h2>
<p>Youghurt, Milk and ice cream processing and
value addition wing is located at the farm where the Farm purchases
milk from other farmers to produce the famous white gold yoghurt</p>
<p>
<img src="{{asset('images/services-2.jpg')}}" alt="" class="img-fluid">
</p>
</div>

@include('sidebar')

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
