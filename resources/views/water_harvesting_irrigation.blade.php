<!DOCTYPE html>
<html lang="en">
<head>
<title>Water harvesting & irrigation | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/advantage/banner.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Water harvesting & irrigation <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Water harvesting & irrigation</h1>
</div>
</div>
</div>
</section>


<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<h3 class="mb-3">Water harvesting</h3>
<p>We supply and build water harvesting dams and reservoirs using the best UV-treated black high density polyethylene (HDPE) dam liners in Kenya manufactured for tropical climates. Our dam liners range from 0.5mm thickens to 1mm thickness and they are welded to fit properly.</p>

<p>Our Water harvesting dams & reservoirs Package;</p>

<ul>
	<li>Quality & Professional end to end work</li>
	<li>HDPE (High density) dam liner supply and installation</li>
	<li>Complete installation with filtration chamber</li>
	<li>Fencing</li>
	<li>Capacity (Up to 5 Million Liters)</li>
	<li>Affordable cost (From KES 120,000)</li>
</ul>

<h3>Irrigation Kit</h3>
<p>Advantage team help you choose the right irrigation system, supply and install irrigation systems depending on your need.</p>
<p>Our irrigation systems are; drip kits, rain hose systems, rain gun sprinklers, impact sprinklers, and pop-up sprinklers. We supply and install various types of irrigation equipment, equipment, and systems.</p>

<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:16%;">
<div class="container">
<div class="row">
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/advantage/2.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/advantage/3.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/advantage/1.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/advantage/4.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/advantage/5.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/advantage/6.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/advantage/7.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/advantage/8.jpg')}});">
</div>
</div>
</div>
</div>
</div>
</section>

<h3>Request for a quotation</h3>

{!! Form::open(['url' => 'request_water_harvesting_irrigation_quotation']) !!}
<div class="row">
<div class="col-md-8 form-group">
<label>Your Name</label>
<input class="form-control" type="text" name="name" value="{{old('name')}}" required>
@if ($errors->has('name'))
   <span class="text-danger">{{ $errors->first('name') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Your Mobile No.</label>
<input class="form-control" type="text" name="mobileNo" value="{{old('mobileNo')}}" required>
@if ($errors->has('mobileNo'))
   <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
<label>Your Location</label>
<input class="form-control" type="text" name="location" value="{{old('location')}}" required>
@if ($errors->has('location'))
   <span class="text-danger">{{ $errors->first('location') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
<label>Details about your request</label>
<textarea class="form-control" name="description" required>{{old('description')}}</textarea>
@if ($errors->has('description'))
   <span class="text-danger">{{ $errors->first('description') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
	<button type="submit" class="btn btn-primary">Send Request</button>
</div>
</div>
</form>

</div>

@include('sidebar')

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
