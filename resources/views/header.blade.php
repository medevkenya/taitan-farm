<div class="top bg-light">
<div class="container">
<div class="row justify-content-between">
<div class="col-sm text-md-left mb-md-0 mt-2 pr-md-4 d-flex topper align-items-center">
<p class="mb-0 w-100 pl-2 pl-md-0">
<span class="fa fa-envelope"></span>
<span class="text">
  <a href="mailto:<?php echo env("HELPEMAIL"); ?>" class="__cf_email__" data-cfemail="96eff9e3e4f3fbf7fffad6f3fbf7fffab8f5f9fb"><?php echo env("HELPEMAIL"); ?></a></span>
</p>
</div>
<!-- <div class="col-sm d-flex mb-md-0 mb-2">
<div class="social-media">
<p class="mb-0 d-flex">
<a href="<?php //echo env("FACEBOOK"); ?>" target="_blank " class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
<a href="<?php //echo env("TWITER"); ?>" target="_blank " class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"><i class="sr-only">Twitter</i></span></a>
<a href="https://api.whatsapp.com/send?phone=<?php //echo env("WHATSAPP"); ?>" target="_blank " class="d-flex align-items-center justify-content-center"><span class="fa fa-whatsapp"><i class="sr-only">Instagram</i></span></a>
</p>
</div>
</div> -->
</div>
</div>
</div>
<div class="pt-4 pb-5">
<div class="container">
<div class="row d-flex align-items-start align-items-center px-3 px-md-0">
<div class="col-md-4 d-flex mb-2 mb-md-0">
<a class="navbar-brand d-flex align-items-center" href="{{URL::to('/')}}">
  <img alt="<?php echo env("APP_NAME"); ?>" src="{{asset('images/logo.png')}}" class="brand-logo">
<!-- <span class="flaticon flaticon-agriculture"></span>
<span class="ml-2">Farmland <small>Agriculture Farming</small></span> -->
</a>
</div>
<div class="col-md-4 d-flex topper mb-md-0 mb-2 align-items-center">
<!-- <div class="icon d-flex justify-content-center align-items-center"> -->
<a href="https://wa.me/message/53MIHYMB2PERO1" target="_blank">
  <img alt="<?php echo env("APP_NAME"); ?>" src="{{asset('icons/whatsapp.png')}}" class="brand-logo">
</a>
<a href="https://www.facebook.com/taitanfarm?mibextid=ZbWKwL" target="_blank">
  <img alt="<?php echo env("APP_NAME"); ?>" src="{{asset('icons/facebook.png')}}" class="brand-logo">
</a>
<a href="https://twitter.com/taitanfarm?t=T4tDnfdgILiH49Bqm25Zfg&s=09" target="_blank">
  <img alt="<?php echo env("APP_NAME"); ?>" src="{{asset('icons/twitter.png')}}" class="brand-logo">
</a>
<a href="https://instagram.com/taitanfarm?igshid=ZDdkNTZiNTM=" target="_blank">
  <img alt="<?php echo env("APP_NAME"); ?>" src="{{asset('icons/instagram.png')}}" class="brand-logo">
</a>
<a href="https://www.tiktok.com/@taitanfarm?_t=8btd3gWUro6&_r=1" target="_blank">
  <img alt="<?php echo env("APP_NAME"); ?>" src="{{asset('icons/tiktok.png')}}" class="brand-logo">
</a>
<!-- </div> -->
</div>
<div class="col-md-4 d-flex topper mb-md-0 align-items-center">
<div class="icon d-flex justify-content-center align-items-center">
  <span class="fa fa-phone">
  </span>
</div>
<div class="text pl-3 pl-md-3">
  <p class="con"><span>Call</span> <span><?php echo env("HELPPHONE"); ?></span></p>
  <p class="con">Call Us Now 24/7 Customer Support</p>
</div>
</div>
</div>
</div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
<div class="container d-flex align-items-center">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
<span class="fa fa-bars"></span> Menu
</button>
<div class="collapse navbar-collapse" id="ftco-nav">
<ul class="navbar-nav mr-auto">
<li class="nav-item {{Request::is('/') ? 'active' :''}}"><a href="{{URL::to('/')}}" class="nav-link">Home</a></li>
<li class="nav-item {{Request::is('avocado_farming') ? 'active' :''}}"><a href="{{URL::to('/avocado_farming')}}" class="nav-link">Avocado Farming</a></li>
<li class="nav-item {{Request::is('farms') ? 'active' :''}}"><a href="{{URL::to('/farms')}}" class="nav-link">Farms</a></li>
<li class="nav-item {{Request::is('products_services') ? 'active' :''}}"><a href="{{URL::to('/products_services')}}" class="nav-link">Products & Services</a></li>
<li class="nav-item {{Request::is('water_harvesting_irrigation') ? 'active' :''}}"><a href="{{URL::to('/water_harvesting_irrigation')}}" class="nav-link">Water & Irrigation</a></li>
<li class="nav-item {{Request::is('agri_financing') ? 'active' :''}}"><a href="{{URL::to('/agri_financing')}}" class="nav-link">Financing Opportunities</a></li>
<li class="nav-item {{Request::is('knowledge_centre') ? 'active' :''}}"><a href="{{URL::to('/knowledge_centre')}}" class="nav-link">Knowledge Centre</a></li>
<li class="nav-item {{Request::is('about_us') ? 'active' :''}}"><a href="{{URL::to('/about_us')}}" class="nav-link">About Us</a></li>
</ul>
<a href="https://play.google.com/store/apps/details?id=farmup.mobile.app" target="_blank" class="btn-custom"></a>
</div>
</div>
</nav>
