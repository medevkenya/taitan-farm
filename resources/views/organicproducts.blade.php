<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image:url(images/organic.jpg)">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/pigfarm')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span class="mr-2"><a href="{{URL::to('/pigfarm')}}">Organic Products
   <i class="fa fa-chevron-right"></i></a></span>

   </p>
<h1 class="mb-0 bread">Organic Products</h1>
</div>
</div>
</div>
</section>
<section class="ftco-section">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">
<h2 class="mb-3">Production Processing</h2>
<p>We do crop production using variours irrigation systems. Variours ranges of crops are produced starting from Tomato Banana, Maize, Soyabean and Ginger </p>
<p>
<img src="images/organic.jpg" alt="" class="img-fluid">
</p>
<!--<p><h2>Beef Farm</h2> The farm has a ranch that currently has 60 Zebu breeds of Sahiwal and Boran cattles for beef purposes.</p>-->

</div>
@include('sidebar')
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
