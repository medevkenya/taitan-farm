<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href=""{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Terms <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Terms</h1>
</div>
</div>
</div>
</section>


		<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Fresh From The Farm</h3>
				<ul class="breadcrumb-tree">
					<!-- <li><a href="#">Taitan Farm</a></li> -->
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<p>
							 Welcome to our website. If you continue to browse and use this website you are agreeing to comply
							 with and be bound by the following terms
							 and conditions of use, which together with our privacy policy govern Taitan Farm relationship with you in relation to this website.</br>
							 The term “Taitan Farm” or “us” or “we” refers to the owner of the website who is Taitan Farm. The term “you” refers to
							 the user or viewer of our website.</br></br>
							 The use of this website is subject to the following terms of use:</br></br>
							 1. The content of the pages of this website is for your general information and use only. It is subject to change without notice. </br></br>
							 2. Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness
							 or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that
							 such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or
							 errors to the fullest extent permitted by law.</br></br>
							 3. Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable.
							 It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</br></br>
							 4. This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look,
							 appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</br></br>
							 5. All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</br></br>
							 6. Unauthorised use of this website may give to a claim for damages and/or be a criminal offence.</br></br>
							 7. From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information.
							 They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</br></br>
							 8. You may not create a link to this website from another website or document without Taitan Farm prior written consent. </br></br>
							 9. Your use of this website and any dispute arising out of such use of the website is subject to the laws of Kenya & the international laws governing such.
						</p>

		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->


@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
