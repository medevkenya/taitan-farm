<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image:url(images/work-1.jpg)">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/pigfarm')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span class="mr-2"><a href="{{URL::to('/pigfarm')}}">Pig Farm
   <i class="fa fa-chevron-right"></i></a></span>

   </p>
<h1 class="mb-0 bread">pig Farm</h1>
</div>
</div>
</div>
</section>
<section class="ftco-section">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">
<h2 class="mb-3"> Large White and Landrace Pigs.</h2>
<p>The farm has 30 pigs that are sold locally. The breeds are hampshire, large white and landrace.</p>
<p>
<img src="images/work-1.jpg" alt="" class="img-fluid">
</p>
<!--<p><h2>Beef Farm</h2> The farm has a ranch that currently has 60 Zebu breeds of Sahiwal and Boran cattles for beef purposes.</p>-->

</div>
@include('sidebar')
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
