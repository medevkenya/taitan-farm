<!DOCTYPE html>
<html lang="en">
<head>
<title>Farms | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Farms <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Farms</h1>
</div>
</div>
</div>
</section>

<section class="ftco-section bg-light">
<div class="container">
<div class="row">
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="#" class="block-20" style="background-image:url(images/image_2.jpg)">
</a>
<div class="text d-block text-center">
<div class="meta">
<p>
<a href="#">6567+ Eggs produced daily</a>
</p>
</div>
<h3 class="heading"><a href="#">Eggcellent</a></h3>
<p>The farm has a subsidiary named Vibe
Brands Limited that produces the popular Vibe Brands</p>
</div>
</div>
</div>
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="#" class="block-20" style="background-image:url(images/bull.jpg)">
</a>
<div class="text d-block text-center">
<div class="meta">
<p>
<a href="#">115+ Bulls</a>
</p>
</div>
<h3 class="heading"><a href="#">Bulls</a></h3>
<p>Some content here...</p>
</div>
</div>
</div>
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="#" class="block-20" style="background-image:url(images/services-3.jpg)">
</a>
<div class="text d-block text-center">
<div class="meta">
<p>
<a href="#">4690+ Chicken</a>
</p>
</div>
<h3 class="heading"><a href="#">Poultry</a></h3>
<p>The poultry section composes of Kienyeji (Indigenous) chicken breed and
improved breeds. The farm has a 520 egg hatchery that produces chicks for sale. The breeds
found are Kenbro, Kienyeji, Raibow rosters, Kuroilers, Geese, Turkeys, Pekins and Ducks</p>
</div>
</div>
</div>
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="#" class="block-20" style="background-image:url(images/work-4.jpg)">
</a>
<div class="text d-block text-center">
<div class="meta">
<p>
<a href="#">115+ Beef Cattles</a>
</p>
</div>
<h3 class="heading"><a href="#">Beef</a></h3>
<p>The farm a ranch that currently has 60 Zebu breeds of Sahiwal and Boran cattles
for beef purposes.</p>
</div>
</div>
</div>
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="#" class="block-20" style="background-image:url(images/services-4.jpg)">
</a>
<div class="text d-block text-center">
<div class="meta">
<p>
<a href="#">250+ Litres daily</a>
</p>
</div>
<h3 class="heading"><a href="#">Dairy</a></h3>
<p>The farm boasts of over 45 high yield dairy cattle. The farm has been selling
heifers to other farmers and acts as a training centre to other farmers on dairy best practices</p>
</div>
</div>
</div>
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="#" class="block-20" style="background-image:url(images/goat.png)">
</a>
<div class="text d-block text-center">
<div class="meta">
<p>
<a href="#">342+ Goats</a>
</p>
</div>
<h3 class="heading"><a href="#">Goats</a></h3>
<p>The farm has 300 Galla goats that are reared in a ranch for beef purposes.</p>
</div>
</div>
</div>
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="#" class="block-20" style="background-image:url(images/pig1.jpg)">
</a>
<div class="text d-block text-center">
<div class="meta">
<p>
<a href="#">46+ Pigs</a>
</p>
</div>
<h3 class="heading"><a href="#">Pigs</a></h3>
<p>The farm has 30 pigs that are sold locally. The breeds are hampshire, large white
and landrace</p>
</div>
</div>
</div>
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
