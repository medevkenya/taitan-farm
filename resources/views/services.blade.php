<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href=""{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Services <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Our Services</h1>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-no-pt ftco-no-pb ftco-about img">
<div class="container">
<div class="row d-flex">
<div class="col-md-12 about-intro">
<div class="row d-flex">
  <div class="col-md-6 pl-md-5 py-5">
  <div class="row justify-content-start pb-3">
  <div class="col-md-12 heading-section ftco-animate">
  <h2 class="mb-4">CROP FARMING</h2>
  <p>Taitan Farm owns separate plots of farms within its werugha location in Taita Taveta We do crop production using various irrigation systems.</p>
  </div>
  </div>
  </div>
<div class="col-md-6 pl-md-5 py-5">
<div class="row justify-content-start pb-3">
<div class="col-md-12 heading-section ftco-animate">
<h2 class="mb-4">LIVESTOCK FARMING</h2>
<p>Taitan Farm has variety of livestock held at company's farms whereby poultry, goats,
   dairy and beef raring make the main livestock project as per now</p>
</div>
</div>
</div>
<div class="col-md-6 pl-md-5 py-5">
<div class="row justify-content-start pb-3">
<div class="col-md-12 heading-section ftco-animate">
<h2 class="mb-4">OUT- GROWERS MANAGEMENT</h2>
<p>Taitan farm manages a list of out growers who produce fertile eggs for hatching at the company</p>
</div>
</div>
</div>
<div class="col-md-6 pl-md-5 py-5">
<div class="row justify-content-start pb-3">
<div class="col-md-12 heading-section ftco-animate">
<h2 class="mb-4">MOBILE AGGROVET & TRANSPORTATION</h2>
<p>Taitan farm Transports hay, feeds & silage to farmers through a mobile Aggrovet.</p>
</div>
</div>
</div>
  <div class="col-md-6 pl-md-5 py-5">
  <div class="row justify-content-start pb-3">
  <div class="col-md-12 heading-section ftco-animate">
  <h2 class="mb-4">TAITAN FARM CONSULTANCY</h2>
  <p>• Farm planning and budgetting<br>
  • Full cost effective constr uction<br>
  • Farm training and management<br>
  • Farm marketing strategy and execution</p>
  </div>
  </div>
  </div>
<div class="col-md-6 pl-md-5 py-5">

</div>
</div>
</div>
</div>
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
