<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm - Fresh from the farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Taitan Farm is an Agro Express company registered in Kenya to carry out agricultural related business including dairy farming, beef, poultry, pig farming, livestock farming, bee keeping, yoghurt production and trading of agricultural products among other Agro Express activities. The company is located and
operates from Werugha location in Wundayi, Taita Taveta County in Kenya.">
<meta name="keywords" content="Taitan Farm, Dairy Farm, Beef Farm, Poultry Farm, Bull Fattening, Goat Farming, White Gold Milk, Eggcellent Eggs Company, Vibe Crips, Taitan Flavors, heifers in Taita Taveta, Agro Express">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
<link rel="canonical" href="<?php echo url()->current(); ?>" />
<meta property="og:locale" content="en_US" />
<meta property="og:title" content="Fresh from the farm | Taitan Farm" />
<meta property="og:description" content="Taitan Farm is an Agro Express company registered in Kenya to carry out agricultural related business including dairy farming, beef, poultry, pig farming, livestock farming, bee keeping, yoghurt production and trading of agricultural products among other Agro Express activities. The company is located and
operates from Werugha location in Wundayi, Taita Taveta County in Kenya." />
<meta property="og:url" content="<?php echo url()->current(); ?>" />
<meta property="og:site_name" content="Taitan Farm" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@taitanfarm"/>
<meta name="twitter:domain" content="taitanfarm.com"/>
<meta name="twitter:creator" content="@taitanfarm">
<meta property="og:type" content="website"/>
<meta property="og:image" content="{{ URL::asset('images/logo.png')}}"/>
<meta name="twitter:image" content="{{ URL::asset('images/logo.png')}}"/>
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap js-fullheight">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
<div class="col-md-8 text-center ftco-animate">
<div class="mb-5">
<span class="subheading">Welcome to Taitan Farm</span>
<h1 class="mb-4">REDIFINING AGRIBUSINESS</h1>
<p><a href="{{URL::to('/water_harvesting_irrigation')}}" class="btn btn-primary">Advantage</a>
  <a href="{{URL::to('/help_me_start_my_farm')}}" class="btn btn-secondary">Start your Farm</a></p>
</div>
</div>
</div>
</div>
<div class="home-slider owl-carousel js-fullheight">
<div class="slider-item js-fullheight" style="background-image:url(images/bg_1.jpg);" data-stellar-background-ratio="0.5">
</div>
<div class="slider-item js-fullheight" style="background-image:url(images/bg_2.jpg);" data-stellar-background-ratio="0.5">
</div>
</div>
</section>
<section class="ftco-section ftco-services ftco-no-pt">
<div class="container">
<div class="row">
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0104.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0102.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0133.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0132.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0035.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0042.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/316274835_596417845818924_5546426389132935061_n.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0018.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0079.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0106.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0137.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/mainpage/IMG-20230429-WA0130.jpg')}});">
</div>
</div>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-no-pt ftco-no-pb ftco-about img">
<div class="container">
<div class="row d-flex">
<div class="col-md-12 about-intro">
<div class="row d-flex">
<div class="col-md-6 d-flex align-items-stretch">
<div class="img d-flex align-items-center align-self-stretch justify-content-center" style="background-image:url(images/about-1.jpg);">
<div class="year-stablish text-center">
<div class="icon2"><span class="flaticon-calendar"></span></div>
<div class="text">
<strong class="number" data-number="06">0</strong><b style="font-weight: 700;
font-size: 32px;
color: #fff;
line-height: 1;
margin-bottom: 5px;">+</b>
<span>Year Of<br> Experience</span>
</div>
</div>
<div class="img-2 d-flex align-items-center justify-content-center" style="background-image:url(images/about-2.jpg);">
</div>
</div>
</div>
<div class="col-md-6 pl-md-5 py-5">
<div class="row justify-content-start pb-3">
<div class="col-md-12 heading-section ftco-animate">
<span class="subheading">About Taitan Farm</span>
<h2 class="mb-4">Biggest youth led mixed farm in coastal Kenya</h2>
<p>Registered in 2018 Taitan Farm Limited is the largest youth led mixed agriculture one stop shop
  in coastal Kenya, Taita Taveta, Wundanyi Town. We have 60 percent women and 80% youths among our
  41 workforce and in leadership. We have driven strong partnership with private organizations,
  associations, non-governmental and governmental organizations to ensure that we impact the
  community first as we meet our investment returns goals. We seek to redefine agriculture
  through innovation, agro-processing, value chain expansion and quality services.
  We only what we have visibility of a steady market and we are sure of good returns.
  We drive our pride by demonstrating to youths how agribusiness can transform Africa,
  creating employment and offering farmer support and opportunities. We have created a
   community of over 1,400 that we provide direct mentorship on.</p>
<p>
  <a href="{{URL::to('/about_us')}}" class="btn btn-secondary">Read More</a>
  <!-- <a href="{{asset('images/TAITANFARMRESUME.pdf')}}" target="_blank" class="btn btn-secondary">Downloads our resume</a> -->
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(images/bg_2.jpg);">
<div class="overlay"></div>
<div class="container">
  <div class="row">
  <div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
  <div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
  <div class="icon d-flex align-items-center justify-content-center">
    <img alt="egg" src="{{asset('images/calendar.png')}}" class="iconimg">
  </div>
  <div class="text pl-3">
  <strong class="number" data-number="2017">0</strong>
  <span>Since</span>
  </div>
  </div>
  </div>
  <div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
  <div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
  <div class="icon d-flex align-items-center justify-content-center">
    <img alt="milk" src="{{asset('images/farmers.png')}}" class="iconimg">
  </div>
  <div class="text pl-3">
  <strong class="number" data-number="1456">0</strong>
  <span>Impact on farmers</span>
  </div>
  </div>
  </div>
  <div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
  <div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
  <div class="icon d-flex align-items-center justify-content-center">
    <img alt="pig" src="{{asset('images/cow.png')}}" class="iconimg">
  </div>
  <div class="text pl-3">
  <strong class="number" data-number="634">0</strong>
  <span>Dairy cows sold</span>
  </div>
  </div>
  </div>
  <div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
  <div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
  <div class="icon d-flex align-items-center justify-content-center">
    <img alt="training" src="{{asset('images/farms.png')}}" class="iconimg">
  </div>
  <div class="text pl-3">
  <strong class="number" data-number="16">0</strong>
  <span>Farms we manage</span>
  </div>
  </div>
  </div>
  </div>
</div>
<div class="container" style="margin-top:2%;">
<div class="row">
<div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
<div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center">
  <img alt="yoghurt" src="{{asset('images/crowd.png')}}" class="iconimg">
</div>
<div class="text pl-3">
<strong class="number" data-number="43">0</strong>
<span>Direct employment</span>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
<div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center">
  <img alt="crowd" src="{{asset('images/icome.png')}}" class="iconimg">
</div>
<div class="text pl-3">
<strong class="number" data-number="256780000">0</strong>
<span>Income to Farmers</span>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
<div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center">
  <img alt="cow" src="{{asset('images/avocado.png')}}" class="iconimg">
</div>
<div class="text pl-3">
<strong class="number" data-number="186">0</strong>+
<span>Avocado farmers</span>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
<div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center">
  <img alt="goat" src="{{asset('images/seedlings.png')}}" class="iconimg">
</div>
<div class="text pl-3">
<strong class="number" data-number="456000">0</strong>
<span>Seedlings</span>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- <section class="ftco-section">
<div class="container">
<div class="row justify-content-center pb-5">
<div class="col-md-12 heading-section text-center ftco-animate">
<span class="subheading">Recent Work</span>
<h2 class="mb-4">Explore Projects</h2>
</div>
</div>
</div>
<div class="container-fluid px-md-4">
<div class="row">
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/bull.jpg);">
<div class="text">
<h3><a href="{{URL::to('/farms')}}">Farms</a></h3>
<a href="{{URL::to('/farms')}}" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/flavors.jpg);">
<div class="text">
<h3><a href="{{URL::to('/taitanflavors')}}">Taitan Flavors</a></h3>
<a href="{{URL::to('/taitanflavors')}}" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/system.png);">
<div class="text">
<h3><a href="{{URL::to('/farmupsystem')}}">Farm System</a></h3>
<a href="{{URL::to('/farmupsystem')}}" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/tftc.PNG);">
<div class="text">
<h3><a href="{{URL::to('/tftc')}}">Taitan Farm Training Centre</a></h3>
<a href="{{URL::to('/tftc')}}" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid px-md-4">
<div class="row">
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/work-2.jpg);">
<div class="text">
<h3><a href="{{URL::to('/vibe')}}">Vibe</a></h3>
<a href="{{URL::to('/vibe')}}" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/about-2.jpg);">
<div class="text">
<h3><a href="{{URL::to('/whitegold')}}">White Gold</a></h3>
<a href="{{URL::to('/whitegold')}}" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/agriexpress.jpg);">
<div class="text">
<h3><a href="{{URL::to('/agroexpress')}}">Agri Express</a></h3>
<a href="{{URL::to('/agroexpress')}}" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>

</div>
</div>
</section> -->


<!-- <hr style="margin: 0;">
<section class="ftco-section ftco-faqs services-section">
<div class="container">
<div class="row d-flex">
<div class="col-lg-6 mb-5 md-md-0 heading-section">
<span class="subheading">Request Quote</span>
<h2 class="mb-5">Request An Estimate</h2>
<div id="writeinfo-home" class="alert alert-success" style="display:none;"></div>
<div class="ajax-loader-home">
  <img src="{{ URL::to('/') }}/public/images/loading.gif" class="img-responsive" />
</div>
<form onsubmit="event.preventDefault();" class="appointment-form ftco-animate">
<div class="">
<div class="form-group">
<input type="text" id="firstName" class="form-control" placeholder="First Name">
</div>
<div class="form-group">
<input type="text" id="lastName" class="form-control" placeholder="Last Name">
</div>
<div class="form-group">
<input type="number" id="mobileNo" class="form-control" placeholder="Phone">
</div>
</div>
<div class="">
<div class="form-group">
<div class="form-field">
<div class="select-wrap">
<div class="icon"><span class="fa fa-chevron-down"></span></div>
<select name="service" id="serviceId" class="form-control">
<option selected disabled>Select a service</option>
<option value="Pigs">Pigs</option>
<option value="Poultry">Poultry</option>
<option value="Dairy">Dairy</option>
<option value="Youghurt">Youghurt</option>
</select>
</div>
</div>
</div>
</div>
<div class="">
<div class="form-group">
<textarea name="messagecontent" id="messagecontent" cols="30" rows="4" class="form-control" placeholder="Message"></textarea>
</div>
<div class="form-group">
<input type="button" id="quotebutton" value="Request A Quote" class="btn btn-primary py-3 px-4">
</div>
</div>
</form>
</div>
<div class="col-lg-6 heading-section pl-lg-5 ftco-animate">
<div class="w-100 mb-4 mb-md-0">
<span class="subheading">Freequesntly Ask Question</span>
<h2 class="mb-5">Frequently Ask Question</h2>
<div id="accordion" class="myaccordion w-100" aria-multiselectable="true">
<div class="card">
<div class="card-header p-0" id="headingOne">
<h2 class="mb-0">
<button href="#collapseOne" class="d-flex align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">
<p class="mb-0">what's your location?</p>
<i class="fa" aria-hidden="true"></i>
</button>
</h2>
</div>
<div class="collapse show" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">
<div class="card-body py-3 px-0">
<p>Our company is located and operates from Werugha location in Wundayi, Taita Taveta County in Kenya.</p>
</div>
</div>
</div>
<div class="card">
<div class="card-header p-0" id="headingTwo" role="tab">
<h2 class="mb-0">
<button href="#collapseTwo" class="d-flex align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
<p class="mb-0">How do I contact Titan Farm for sales?</p>
<i class="fa" aria-hidden="true"></i>
</button>
</h2>
</div>
<div class="collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">
<div class="card-body py-3 px-0">
<ol>
<li>You can talk to us on (+245)73577770</li>
<li>P.O BOX 31-80300 VOI, KENYA</li>
<li>Email us on: taitanfarm@gmail.com</li>
</ol>
</div>
</div>
</div>
<div class="card">
<div class="card-header p-0" id="headingThree" role="tab">
<h2 class="mb-0">
<button href="#collapseThree" class="d-flex align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseThree">
<p class="mb-0">Are you open for a partnership?</p>
<i class="fa" aria-hidden="true"></i>
</button>
</h2>
</div>
<div class="collapse" id="collapseThree" role="tabpanel" aria-labelledby="headingTwo">
<div class="card-body py-3 px-0">
<p>Taitan farm endeavors to connect with farmers at diffrent levels in the Agro Express industry to share knowledge and resources as the catapult to shared growth. We have opened up to partnerships volunteerism and mentorship with like-minded stakeholders in the production chains.</p>
</div>
</div>
</div>
<div class="card">
<div class="card-header p-0" id="headingFour" role="tab">
<h2 class="mb-0">
<button href="#collapseFour" class="d-flex align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseFour">
<p class="mb-0">Do you deliver across the country?</p>
<i class="fa" aria-hidden="true"></i>
</button>
</h2>
</div>
<div class="collapse" id="collapseFour" role="tabpanel" aria-labelledby="headingTwo">
<div class="card-body py-3 px-0">
<p>Taitan farm Transports hay, feeds & silage to farmers through a
mobile Aggrovet.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section> -->
<!-- <section class="ftco-section bg-light">
<div class="container">
<div class="row justify-content-center pb-4">
<div class="col-md-12 heading-section text-center ftco-animate">
<span class="subheading">Our Blog</span>
<h2 class="mb-4">Recent Posts</h2>
</div>
</div>
<div class="row">
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="{{URL::to('/vibe')}}" class="block-20" style="background-image: url('images/image_1.jpg');">
</a>
<div class="text d-block text-center">
<h3 class="heading"><a href="{{URL::to('/vibe')}}">Vibe Crips</a></h3>
<p>The farm has a subsidiary named Vibe Brands Limited that produces the popular Vibe Brands.</p>
<p class="mb-0"><a href="{{URL::to('/vibe')}}" class="btn-custom">Read more</a></p>
</div>
</div>
</div>
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="{{URL::to('/farms')}}" class="block-20" style="background-image: url('images/image_2.jpg');">
</a>
<div class="text d-block text-center">
<h3 class="heading"><a href="{{URL::to('/farms')}}">Eggcellent Eggs Company</a></h3>
<p>The farm has over 4,000-layer birds that produce eggs that are packaged under
eggcellent brand.</p>
<p class="mb-0"><a href="{{URL::to('/farms')}}" class="btn-custom">Read more</a></p>
</div>
</div>
</div>
<div class="col-md-4 ftco-animate">
<div class="blog-entry">
<a href="{{URL::to('/farms')}}" class="block-20" style="background-image: url('images/bull.jpg');">
</a>
<div class="text d-block text-center">
<h3 class="heading"><a href="{{URL::to('/farms')}}">Dairy Farm</a></h3>
<p>The farm has been selling heifers to other farmers and acts as a training centre to other farmers on dairy best practices.</p>
<p class="mb-0"><a href="{{URL::to('/farms')}}" class="btn-custom">Read more</a></p>
</div>
</div>
</div>
</div>
</div>
</section> -->

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
