<p>We offer the following services;</p>
<ul>
	<li><a href="{{URL::to('/help_me_start_my_farm')}}">Consultancy and farm set up</a></li>
	<li><a href="{{URL::to('/avocado_farming')}}">Agronomy services</a></li>
	<li><a href="{{URL::to('/help_me_start_my_farm')}}">Water harvesting, irrigation and drilling solutions</a></li>
	<li><a href="{{URL::to('/agri_financing')}}">Agribusiness market opportunities</a></li>
	<li><a href="{{URL::to('/agri_financing')}}">Agribusiness Financing opportunities</a></li>
	<li><a href="{{URL::to('/help_me_start_my_farm')}}">Soil tests and analysis</a></li>
	<li><a href="{{URL::to('/dairy_farm')}}">Dairy farming training and construction</a></li>
</ul>

<p>Our products are;</p>
<ul>
	<li><a href="{{URL::to('/dairy_farm')}}">Quality dairy animals</a></li>
	<li><a href="{{URL::to('/avocado_farming')}}">Avocado farm set up & orchards</a></li>
	<li><a href="{{URL::to('/avocado_farming')}}">Avocado seedlings & farm management</a></li>
	<li><a href="{{URL::to('/avocado_farming')}}">Avocado exporting</a></li>
	<li><a href="{{URL::to('/taitan_kennels')}}">German shepherd puppies</a></li>
	<li><a href="{{URL::to('/help_me_start_my_farm')}}">Apiaries and beehives</a></li>
	<li><a href="{{URL::to('/help_me_start_my_farm')}}">Bull fattening, Poultry & Pork</a></li>
	<li><a href="{{URL::to('/products_services')}}">Processing of milk, avocado oil & honey</a></li>
	<li><a href="{{URL::to('/dairy_farm')}}">Dairy inputs like fodder and silage delivered to you</a></li>
</ul>
