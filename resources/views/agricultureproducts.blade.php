<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image:url(images/bull.jpg)">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/pigfarm')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span class="mr-2"><a href="{{URL::to('/agricultureproducts')}}">Agriculture Products
   <i class="fa fa-chevron-right"></i></a></span>

   </p>
<h1 class="mb-0 bread">Agriculture Products</h1>
</div>
</div>
</div>
</section>
<section class="ftco-section">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">
<h2 class="mb-3">Crop Farming</h2>
<p>Taitan Farm owns separate plots of farms within its werugha location in Taita Taveta We do crop production using various irrigationsystems.</p>
<p>
<img src="images/agricss.jpg" alt="" class="img-fluid">
</p>
<p><h2>Livestoke Farming</h2> Taitan Farm has variety of livestock held at company's farms whereby poultry, goats, dairy and beef raring make the main livestock project for the time being.</p>

</div>
@include('sidebar')
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
