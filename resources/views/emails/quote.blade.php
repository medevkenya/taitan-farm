<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style>
    html, body {font-size: 16px;}
</style>
<table style="background:#00AEED;width:100%;border-radius:4px;padding-left:2%;">
<p style="color:#000000;">
Dear Taitan Farm Admin,
</p>
<p style="color:#000000;">
Find below request for quote
</p>
<p style="color:#000000;">
<strong>Name:</strong> {{name}}
</p>
<p style="color:#000000;">
<strong>Phone:</strong> {{phone}}
</p>
<p style="color:#000000;">
<strong>Service:</strong> {{service}}
</p>
<p style="color:#000000;">
<strong>Message:</strong> {{messagecontent}}
</p>
<br>
<a href="https://taitanfarm.com/" style="text-decoration:none;background-color: #FFA500;
    color: #000000;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    border-radius: 4px;
    width: 100%;">Click to open website</a>
<br><br>
<p>We request you to add our email to your address book to avoid missing out on information that we send to you from time to time.</p>
<p style="color:#000000;">
Thank you for choosing Taitan Farm.
</p>
<p style="color:#000000;">All emails sent to or from Taitan Farm are subject to Taitan Farm's Terms & Conditions of use.</p>
<p style="color:#000000;">&copy; All rights reserved</p>
</table>
