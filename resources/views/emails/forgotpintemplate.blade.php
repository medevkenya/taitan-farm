<table style="background: #FFFFFF;
color: #000000;width:100%;border-radius:4px;padding:2%;">
<img src="{{ URL::to('/') }}/images/logo.png" style="width:15%;"/>
<p style="color:#000000;">
Dear {{ $name }},
</p>
<p style="color:#000000;">
Click on the link below to reset your Account Password.
</p>

<a href="{{ url('/') }}/ResetPasswordLink/{{ $pass }}/{{ $email }}" style="text-decoration:none;background-color: #225253;
  border: 1px solid #225253;
  color: #fff;
  padding: 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;border-radius:6px;
  margin: 4px 2px;"><b style="color:#fff;">CLICK TO RESET PASSWORD</b></a>

<hr>
<p style="color:#000000;">
Thank you for choosing AlternetSMS.
</p>
<p style="color:#000000;">All emails sent to or from AlternetSMS are subject to our Terms & Conditions of use.</p>
<p style="color:#000000;">&copy; All rights reserved</p>
</table>
