<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href=""{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Privacy Policy<i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Privacy Policy</h1>
</div>
</div>
</div>
</section>

<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<!-- <h3 class="breadcrumb-header">Privacy Policy</h3> -->
				<ul class="breadcrumb-tree">
					<!-- <li><a href="#">Taitan Farm</a></li> -->
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<style>
			h5 {
				margin-top: 4%;
			}
			</style>

			<h5 style="margin-top:-2%;">Notice</h5>
						Please read these terms and conditions carefully. The following terms and conditions govern your access and use of this website. By accessing, using, printing, installing or downloading any material from this site and any pages thereof, you agree to be bound by the terms and conditions below.  If you do not agree to the terms and conditions below, do not access this site, or any pages thereof.

						<h5>Use of Information and Materials</h5>
						The information and materials contained in these pages, and the terms, conditions, and descriptions that appear, are subject to change. Unauthorised use of Taitan Farm’s web sites and systems including but not limited to unauthorised entry into Taitan Farm’s systems, misuse of passwords, or misuse of any information posted on a site is strictly prohibited.  Your eligibility for particular services is subject to final determination by Taitan Farm						<h5>Links to Other Sites</h5>
						This site may contain links to web sites controlled or offered by third parties. Taitan Farm hereby disclaims liability for, any information, materials, and products or services posted or offered at any of the third party sites linked to this website.  By creating a link to a third party web site, Taitan Farm does not endorse or recommend any products or services offered, or information contained at that web site, nor is Taitan Farm liable for any failure of products or services offered or advertised at those sites.  Such third parties may have a privacy policy different from that of Taitan Farm and the third party website may provide less security than the Taitan Farm site.
						<h5>No Warranty</h5>
						The information and materials contained in this site, including text, graphics, links or other items are provided as is, as available.  Taitan Farm does not warrant the accuracy, adequacy or completeness of this information and materials and expressly disclaims liability for errors or omissions in this information and materials.  No warranty of any kind, implied, expressed or statutory including but not limited to the warranties of non-infringement of third party rights, title, merchantability, fitness for a particular purpose and freedom from computer virus, is given in conjunction with the information and materials.
						<h5>Limitation of Liability</h5>
						In no event will Taitan Farm be liable for any damages, including without limitation direct or indirect, special, incidental, or consequential damages, losses or expenses arising in connection with this site or any linked site or use thereof or inability to use by any party, or in connection with any failure of performance, error, omission, interruption, defect, delay in operation or transmission, computer virus or line or system failure, even if Taitan Farm, or representatives thereof, are advised of the possibility of such damages, losses or expenses.
						<h5>Submissions</h5>
						All information submitted to Taitan Farm via this site shall be deemed and remain the property of Taitan Farm.  Taitan Farm shall be free to use, for any purpose, any idea, concepts, know-how or techniques contained in information a visitor to this site provides Taitan Farm through this site.  Taitan Farm shall not be subject to any obligations of confidentiality regarding submitted information except specifically agreed or required by law.  Nothing contained herein shall be construed as limiting or reducing Taitan Farm’s responsibilities and obligations to customers in accordance with the Taitan Farm’s privacy promise for consumers.
						<h5>Availability</h5>
						This site is not intended for distribution to, or use by, any person or entity in any jurisdiction or country where such distribution or use would be contrary to local law or regulation.  By offering this site and information, or any products or services via this site, no distribution or solicitation is made by Taitan Farm to any person to use this site, or such information, products or services in jurisdictions where the provision of this site and such information, products and services is prohibited by law.
						<h5>Additional Terms</h5>
						Certain sections or pages on this site may contain separate terms and conditions, which are in addition to these terms and conditions.  In the event of a conflict, the additional terms and conditions will govern for those sections or pages.
						<h5>Governing Law and Jurisdiction</h5>
						These terms and conditions and all matters arising out of or relating to the use of this site shall be governed by and are to be construed in accordance with the laws of the Republic of Kenya.  By accessing this site, you hereby consent to the exclusive jurisdiction of the Kenyan courts in all disputes arising out of or relating to the use of this site.
						&copy; Taitan Farm 2021. All rights reserved
						<br>
						Copyright in the pages and in the screens displaying the pages, and in the information and material therein and in their arrangement, is owned by Taitan Farm unless otherwise indicated


		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
