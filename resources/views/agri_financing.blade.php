<!DOCTYPE html>
<html lang="en">
<head>
<title>Agri Financing | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/TaitanKennels/banner.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Agri Financing <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Agri Financing</h1>
</div>
</div>
</div>
</section>


<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<!-- <h2 class="mb-3">Agri Financing</h2> -->
<p>Grants and opportunities</p>

<div class="row">
<div class="categories">
<ul>
<?php foreach ($list as $key_) { ?>

<?php if(!empty($key_->link)) { ?>
<li>
<div class="col-md-12">
<a href="{{$key_->link}}" target="_blank">{{$key_->title}}</a>
</div>
</li>
<?php } ?>

<?php if(!empty($key_->photo)) { ?>
<li>
<div class="col-md-4">
<p>{{$key_->title}}</p>
<img src="{{ URL::to('/') }}/photos/<?php echo $key_->photo; ?>" alt="" class="img-fluid">
</div>
</li>
<?php } ?>

<?php if(!empty($key_->pdf)) { ?>
<li>
	<div class="col-md-12">
	<a href="{{ URL::to('/') }}/photos/{{$key_->pdf}}" target="_blank">{{$key_->title}}</a>
	</div>
</li>
<?php } ?>

<?php } ?>
</ul>
</div>
</div>

</div>

@include('sidebar')

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
