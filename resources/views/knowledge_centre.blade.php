<!DOCTYPE html>
<html lang="en">
<head>
<title>Knowledge Centre | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

	<div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v16.0&appId=553089122682860&autoLogAppEvents=1" nonce="QoIp1dkv"></script>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/mainpage/knowledge.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Knowledge Centre <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Knowledge Centre</h1>
</div>
</div>
</div>
</section>


<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<!-- <h2 class="mb-3">Knowledge Centre</h2> -->
<p>Learn more about agribusiness</p>

<div class="row">
	<?php foreach ($list as $key) { ?>

		<div class="col-md-4">
		<div class="services">
		<div class="p-4">
		<a href="<?php $url = URL::to("/knowledgecentre/".$key->slug); print_r($url); ?>">
		<div class="media-body">
		<h3 class="heading mb-3"><?php echo $key->title; ?></h3>
		<?php echo substr($key->description, 0, 120) . '...' ; ?>
		</div>
		</a>
		</div>
		<a href="<?php $url = URL::to("/knowledgecentre/".$key->slug); print_r($url); ?>">
		<div class="img" style="background-image: url({{ URL::to('/') }}/photos/<?php echo $key->photo; ?>);">
		</a>
		<a href="<?php $url = URL::to("/knowledgecentre/".$key->slug); print_r($url); ?>" class="btn-custom d-flex align-items-center justify-content-center">
			<span class="fa fa-chevron-right"></span>
		</a>
		</div>
		</div>
		</div>

	<?php } ?>
</div>

<div class="row">
<div class="col-md-12">
<div class="fb-page" data-href="https://www.facebook.com/taitanfarm/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/taitanfarm/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/taitanfarm/">Taitan Farm</a></blockquote></div>
</div>
</div>

</div>

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
