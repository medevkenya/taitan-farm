<!DOCTYPE html>
<html lang="en">
<head>
<title>Bulls, Meat &Taitan Flavors | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/MeatTaitanFlavors/banner.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Bulls, Meat &Taitan Flavors <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Bulls, Meat &Taitan Flavors</h1>
</div>
</div>
</div>
</section>


<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<!-- <h2 class="mb-3">Bulls, Meat &Taitan Flavors</h2> -->
<p>Taitan flavors has the following sections;</p>
<ul>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Layers and poultry section with improved local chicken breeds, broiler chicken and layers of red Rhode Island with egg production capacity of 2,000 eggs per day" target="_blank">
			Layers and poultry section with improved local chicken breeds, broiler chicken and layers of red Rhode Island with egg production capacity of 2,000 eggs per day
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Bull fattening of 45 bulls holding capacity" target="_blank">
		Bull fattening of 45 bulls holding capacity
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Pig section where the farm holds over 90 pigs for sell of pork and piglets to farmers" target="_blank">
			Pig section where the farm holds over 90 pigs for sell of pork and piglets to farmers
		</a>
	</li>
</ul>

<h3>Make an order now</h3>

{!! Form::open(['url' => 'request_bulls_meat_taitan_flavours_quotation']) !!}
<div class="row">
<div class="col-md-4 form-group">
<label>Your Name</label>
<input class="form-control" type="text" name="name" value="{{old('name')}}" required>
@if ($errors->has('name'))
   <span class="text-danger">{{ $errors->first('name') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Your Mobile No.</label>
<input class="form-control" type="text" name="mobileNo" value="{{old('mobileNo')}}" required>
@if ($errors->has('mobileNo'))
   <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Your Location</label>
<input class="form-control" type="text" name="location" value="{{old('location')}}" required>
@if ($errors->has('location'))
   <span class="text-danger">{{ $errors->first('location') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
<label>Details about your order</label>
<textarea class="form-control" name="description" required>{{old('description')}}</textarea>
@if ($errors->has('description'))
   <span class="text-danger">{{ $errors->first('description') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
	<button type="submit" class="btn btn-primary">Send Request</button>
</div>
</div>
</form>

</div>

@include('sidebar')

<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:10%;">
<div class="container">
<div class="row">
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/MeatTaitanFlavors/IMG-20230429-WA0130.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/MeatTaitanFlavors/IMG-20230429-WA0059.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/MeatTaitanFlavors/IMG-20230429-WA0067.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/MeatTaitanFlavors/IMG-20230429-WA0075.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/MeatTaitanFlavors/IMG-20230429-WA0077.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/MeatTaitanFlavors/IMG-20230429-WA0079.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/MeatTaitanFlavors/IMG-20230429-WA0093.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/MeatTaitanFlavors/IMG-20230429-WA0097.jpg')}});">
</div>
</div>
</div>
</div>
</div>
</section>

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
