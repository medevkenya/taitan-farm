<footer class="ftco-footer">
<div class="container">
<div class="row mb-5 justify-content-between">
<div class="col-sm-12 col-md">
<div class="ftco-footer-widget mb-4">
<h2 class="ftco-heading-2 logo"><a href="#">Taitan Farm</a></h2>
<p>Registered in 2018 Taitan Farm Limited is the largest youth led mixed agriculture one stop shop in coastal Kenya, Taita Taveta, Wundanyi Town. We have 60 percent women and 80% youths among our 41.... <a href="{{URL::to('/about_us')}}">Read more</a></p>
<ul class="ftco-footer-social list-unstyled mt-2">
<li class="ftco-animate"><a href="<?php echo env("TWITER"); ?>"target="_blank"><span class="fa fa-twitter"></span></a></li>
<li class="ftco-animate"><a href="<?php echo env("FACEBOOK"); ?>"target="_blank"><span class="fa fa-facebook"></span></a></li>
<li class="ftco-animate"><a href="https://api.whatsapp.com/send?phone=<?php echo env("WHATSAPP"); ?>"target="_blank"><span class="fa fa-whatsapp"></span></a></li>
</ul>
</div>
</div>
<div class="col-sm-12 col-md-4">
<div class="ftco-footer-widget mb-4 ml-md-4">
<h2 class="ftco-heading-2">Quick Links</h2>

<ul class="list-unstyled">
  <li class="nav-item"><a href="{{URL::to('/')}}" class="nav-link">Home</a></li>
  <li class="nav-item"><a href="{{URL::to('/avocado_farming')}}" class="nav-link">Avocado Farming</a></li>
  <li class="nav-item"><a href="{{URL::to('/dairy_farm')}}" class="nav-link">Dairy Farming</a></li>
  <li class="nav-item"><a href="{{URL::to('/products_services')}}" class="nav-link">Products & Services</a></li>
  <li class="nav-item"><a href="{{URL::to('/agri_financing')}}" class="nav-link">Financing Opportunities</a></li>
</ul>

</div>
</div>
<div class="col-sm-12 col-md-2">
<div class="ftco-footer-widget mb-4">
<h2 class="ftco-heading-2">Quick Links</h2>
<ul class="list-unstyled">
  <li class="nav-item"><a href="{{URL::to('/water_irrigation')}}" class="nav-link">Water & Irrigation</a></li>
  <li class="nav-item"><a href="{{URL::to('/help_me_start_my_farm')}}" class="nav-link">Start your farm</a></li>
  <li class="nav-item"><a href="{{URL::to('/about_us')}}" class="nav-link">About</a></li>
  <li class="nav-item"><a href="{{URL::to('/terms')}}" class="nav-link">Terms Of Use</a></li>
  <li class="nav-item"><a href="{{URL::to('/privacy')}}" class="nav-link">Privacy Policy</a></li>
</ul>
</div>
</div>
<div class="col-sm-12 col-md">
<div class="ftco-footer-widget mb-4">
<h2 class="ftco-heading-2">Have a Questions?</h2>
<div class="block-23 mb-3">
<ul>
<li><span class="icon fa fa-map marker"></span><span class="text">Werugha Wundayi, Taita Taveta County, Kenya.</span></li>
<li><a href="#"><span class="icon fa fa-phone"></span><span><?php echo env("HELPPHONE"); ?></span></a></li>
<li><a href="mailto:<?php echo env("HELPEMAIL"); ?>"><span class="icon fa fa-paper-plane pr-4"></span><span> <?php echo env("HELPEMAIL"); ?></span></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid px-0 py-5 bg-black">
<div class="container">
<div class="row">
<div class="col-md-12">
  <p class="mb-0" style="color: rgba(255,255,255,.5);">Copyright &copy;
    <script>document.write(new Date().getFullYear());</script>
    All rights reserved | <?php echo env('APP_NAME'); ?>
  </p>
</div>
</div>
</div>
</div>
</footer>
