<!DOCTYPE html>
<html lang="en">
<head>
<title>Farm-up Systems | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>About us <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Farm-up Systems</h1>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-no-pt ftco-no-pb ftco-about img">
<div class="container">
<div class="row d-flex">
<div class="col-md-12 about-intro">
<div class="row d-flex">
<div class="col-md-6 d-flex align-items-stretch" style="max-height:700px;">
<div class="img d-flex align-items-center align-self-stretch justify-content-center" style="background-image:url(images/app1.png)">
<div class="year-stablish text-center">
<div class="icon2"><span class="flaticon-calendar"></span></div>
<div class="text">
<strong class="number" data-number="1000">0</strong>
<span>Downloads</span>
</div>
</div>
<div class="img-2 d-flex align-items-center justify-content-center" style="background-image:url(images/app2.png)">
</div>
</div>
</div>
<div class="col-md-6 pl-md-5 py-5">
<div class="row justify-content-start pb-3">
<div class="col-md-12 heading-section ftco-animate">
<span class="subheading">Android mobile app</span>
<h2 class="mb-4">Farm-Up System Mobile App</h2>
<p>This is a smart farm management mobile app. The farm has developed a farm management system and a farm market that assists farmers to manage
their and also sell and buy farm produce on the platform which is an android application.</p>

<h3>Key features</h3>
<p>Poultry and Dairy management</p>
<p>Customers & Suppliers management</p>
<p>Debtors and creditors records</p>
<p>Farm market</p>
<p>Births and mortalities</p>
<p>Health records, AI records & Vaccinations</p>
</div>
<p><a href="https://play.google.com/store/apps/details?id=farmup.mobile.app" target="_blank" class="btn btn-secondary">Click to download our app</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
