<section class="ftco-intro img" style="background-image: url({{ URL::to('/') }}/images/bg_4.jpg);">
<div class="overlay"></div>
<div class="container">
<div class="row justify-content-center">
<div class="col-md-12 heading-section heading-section-white text-center ftco-animate">
<h2>Subscribe to our Newsletter</h2>
</div>
</div>
<div class="row justify-content-center">
<div class="col-md-6">
<form onsubmit="event.preventDefault();" class="subscribe-form ftco-animate">
  <div id="writeinfo" class="alert alert-success" style="display:none;"></div>
  <div class="ajax-loader">
    <img src="{{ URL::to('/') }}/public/images/loading.gif" class="img-responsive" />
  </div>
<div class="form-group d-flex">
<input type="email" name="email" id="subscribeemail" class="form-control" placeholder="Enter email address">
<input type="button" value="Subscribe" id="subscribebutton" class="submit px-3">
</div>
</form>
</div>
</div>
</div>
</section>
