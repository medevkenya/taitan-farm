<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span> <span>Reviews & Partnership<i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Reviews & Partnership</h1>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-no-pt ftco-no-pb ftco-about img">
<div class="container">
<div class="row d-flex">
<div class="col-md-12 about-intro">
<div class="row d-flex">
<div class="col-md-6 d-flex align-items-stretch">
<div class="img d-flex align-items-center align-self-stretch justify-content-center" style="background-image:url(images/BG_2.jpg)">
<!--<div class="year-stablish text-center">
<div class="icon2"><span class="flaticon-calendar"></span></div>
<div class="text">
<strong class="number" data-number="05">0</strong>
<span>Year Of<br> Experience</span>
</div>
</div>-->
<div class="img-2 d-flex align-items-center justify-content-center" style="background-image:url(images/about-2.jpg)">
</div>
</div>
</div>
<div class="col-md-6 pl-md-5 py-5">
<div class="row justify-content-start pb-3">
<div class="col-md-12 heading-section ftco-animate">
<span class="subheading">Agro Express</span>
<h2 class="mb-4">Our partnership with AgriBiz</h2>
<p>AgriBiz is an economic empowerment programme that primarily targets rural and peri-urban youth and women who are under employed and lack the financial resources and know-how to create viable sustainable businesses<br><a href="https://agribiz.kenyacic.org/tag/taitan-farm/"target="_blank " class="btn btn-secondary">Read More</a></p>
<!--<div class="row my-4">
<div class="col-md-6 ftco-animate">
<div class="services-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-agriculture"></span></div>
<div class="media-body">
<h3 class="heading">Growing Fruits<br> and Vegetables</h3>
</div>
</div>
</div>
<div class="col-md-6 ftco-animate">
<div class="services-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-agriculture-2"></span></div>
<div class="media-body">
<h3 class="heading">Tips for Ripening<br> Fruits</h3>
</div>
</div>
</div>
</div>-->
</div>
<!--<p><a href="#" class="btn btn-secondary">Learn More</a></p>-->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(images/bg_2.jpg);">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
<div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-agriculture-1"></span></div>
<div class="text pl-3">
<strong class="number" data-number="60">0</strong>
<span>Zebu breeds</span>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
<div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-agriculture"></span></div>
<div class="text pl-3">
<strong class="number" data-number="520">0</strong>
<span>egg hatchery</span>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
<div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-agriculture-2"></span></div>
<div class="text pl-3">
<strong class="number" data-number="30">0</strong>
<span>pigs</span>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 d-flex counter-wrap ftco-animate">
<div class="block-18 mb-xl-0 mb-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-approve"></span></div>
<div class="text pl-3">
<strong class="number" data-number="300">0</strong>
<span>Galla goats</span>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="video-image img" style="background-image: url(images/bg_3.jpg);">
<div class="overlay-2"></div>
<div class="overlay"></div>
<div class="container">
<div class="row justify-content-center align-items-center wrap-video">
<div class="col-md-6 text-center">
<a href="https://vimeo.com/45830194" class="icon-video popup-vimeo d-flex align-items-center justify-content-center mb-4">
<span class="fa fa-play"></span>
</a>
<h3>Watch Modern Agricultural Farming</h3>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-no-pt testimony-section">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-6 heading-section pr-md-5 pt-5 mt-md-5 mb-5 mb-md-0">
<span class="subheading">Testimonial</span>
<h2 class="mb-4">What Are Cutomers Says About</h2>
<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
<div class="block-18 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-agriculture"></span></div>
<div class="text pl-3">
<strong class="number" data-number="30587">0</strong>
<span>Total Products</span>
</div>
</div>
</div>
<div class="col-md-6 pl-md-5 d-flex align-items-stretch">
<div class="carousel-testimony owl-carousel d-flex align-items-center">
<div class="item">
<div class="testimony-wrap">
<div class="text">
<span class="fa">"</span>
<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
<div class="d-flex align-items-center">
<div class="user-img" style="background-image: url(images/person_1.jpg)"></div>
<div class="pl-3">
<p class="name">Roger Scott</p>
<span class="position">Marketing Manager</span>
</div>
</div>
</div>
</div>
</div>
<div class="item">
<div class="testimony-wrap">
<div class="text">
<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
<div class="d-flex align-items-center">
<div class="user-img" style="background-image: url(images/person_2.jpg)"></div>
<div class="pl-3">
<p class="name">Roger Scott</p>
<span class="position">Marketing Manager</span>
</div>
</div>
</div>
</div>
</div>
<div class="item">
<div class="testimony-wrap">
<div class="text">
<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
<div class="d-flex align-items-center">
<div class="user-img" style="background-image: url(images/person_3.jpg)"></div>
<div class="pl-3">
<p class="name">Roger Scott</p>
<span class="position">Marketing Manager</span>
</div>
</div>
</div>
</div>
</div>
<div class="item">
<div class="testimony-wrap">
<div class="text">
<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
<div class="d-flex align-items-center">
<div class="user-img" style="background-image: url(images/person_1.jpg)"></div>
<div class="pl-3">
<p class="name">Roger Scott</p>
<span class="position">Marketing Manager</span>
</div>
</div>
</div>
</div>
</div>
<div class="item">
<div class="testimony-wrap">
<div class="text">
<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
<div class="d-flex align-items-center">
<div class="user-img" style="background-image: url(images/person_2.jpg)"></div>
<div class="pl-3">
<p class="name">Roger Scott</p>
<span class="position">Marketing Manager</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<hr style="margin: 0;">
<section class="ftco-section ftco-faqs services-section">
<div class="container">
<div class="row d-flex">
<div class="col-lg-6 mb-5 md-md-0 heading-section">
<span class="subheading">Request Quote</span>
<h2 class="mb-5">Request An Estimate</h2>
<form action="#" class="appointment-form ftco-animate">
<div class="">
<div class="form-group">
<input type="text" class="form-control" placeholder="First Name">
</div>
<div class="form-group">
<input type="text" class="form-control" placeholder="Last Name">
</div>
<div class="form-group">
<input type="text" class="form-control" placeholder="Phone">
</div>
</div>
<div class="">
<div class="form-group">
<div class="form-field">
<div class="select-wrap">
<div class="icon"><span class="fa fa-chevron-down"></span></div>
<select name="" id="" class="form-control">
<option value="">Select Your Services</option>
<option value="">Organic Solution</option>
<option value="">Harvest Innovation</option>
<option value="">Farm System</option>
<option value="">Agriculture Farming</option>
<option value="">Other Services</option>
</select>
</div>
</div>
</div>
</div>
<div class="">
<div class="form-group">
<textarea name="" id="" cols="30" rows="4" class="form-control" placeholder="Message"></textarea>
</div>
<div class="form-group">
<input type="submit" value="Request A Quote" class="btn btn-primary py-3 px-4">
</div>
</div>
</form>
</div>
<div class="col-lg-6 heading-section pl-lg-5 ftco-animate">
<div class="w-100 mb-4 mb-md-0">
<span class="subheading">Freequesntly Ask Question</span>
<h2 class="mb-5">Frequently Ask Question</h2>
<div id="accordion" class="myaccordion w-100" aria-multiselectable="true">
<div class="card">
<div class="card-header p-0" id="headingOne">
<h2 class="mb-0">
<button href="#collapseOne" class="d-flex align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">
<p class="mb-0">what's your location?</p>
<i class="fa" aria-hidden="true"></i>
</button>
</h2>
</div>
<div class="collapse show" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">
<div class="card-body py-3 px-0">
<p>Our company is located and operates from Werugha location in Wundayi, Taita Taveta County in Kenya.</p>
</div>
</div>
</div>
<div class="card">
<div class="card-header p-0" id="headingTwo" role="tab">
<h2 class="mb-0">
<button href="#collapseTwo" class="d-flex align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
<p class="mb-0">How do I contact Titan Farm for sales?</p>
<i class="fa" aria-hidden="true"></i>
</button>
</h2>
</div>
<div class="collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">
<div class="card-body py-3 px-0">
<ol>
<li>You can talk to us on (+245)73577770</li>
<li>P.O BOX 31-80300 VOI, KENYA</li>
<li>Email us on: taitanfarm@gmail.com</li>
</ol>
</div>
</div>
</div>
<div class="card">
<div class="card-header p-0" id="headingThree" role="tab">
<h2 class="mb-0">
<button href="#collapseThree" class="d-flex align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseThree">
<p class="mb-0">Are you open for a partnership?</p>
<i class="fa" aria-hidden="true"></i>
</button>
</h2>
</div>
<div class="collapse" id="collapseThree" role="tabpanel" aria-labelledby="headingTwo">
<div class="card-body py-3 px-0">
<p>Taitan farm endeavors to connect with farmers at diffrent levels in the agri-business industry to share knowledge and resources as the catapult to shared growth. We have opened up to partnerships volunteerism and mentorship with like-minded stakeholders in the production chains.</p>
</div>
</div>
</div>
<div class="card">
<div class="card-header p-0" id="headingFour" role="tab">
<h2 class="mb-0">
<button href="#collapseFour" class="d-flex align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseFour">
<p class="mb-0">Do you deliver across the country?</p>
<i class="fa" aria-hidden="true"></i>
</button>
</h2>
</div>
<div class="collapse" id="collapseFour" role="tabpanel" aria-labelledby="headingTwo">
<div class="card-body py-3 px-0">
<p>Taitan farm Transports hay, feeds & silage to farmers through a
mobile Aggrovet.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>-->
<section class="ftco-intro img" style="background-image: url(images/bg_4.jpg);">
<div class="overlay"></div>
<div class="container">
<div class="row justify-content-center">
<div class="col-md-12 heading-section heading-section-white text-center ftco-animate">
<h2>Subscribe to our Newsletter</h2>
</div>
</div>
<div class="row justify-content-center">
<div class="col-md-6">
<form action="#" class="subscribe-form ftco-animate">
<div class="form-group d-flex">
<input type="text" class="form-control" placeholder="Enter email address">
<input type="submit" value="Subscribe" class="submit px-3">
</div>
</form>
</div>
</div>
</div>
</section>

@include('footer')

@include('footerlinks')

</body>
</html>
