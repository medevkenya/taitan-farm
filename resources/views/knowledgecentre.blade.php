<!DOCTYPE html>
<html lang="en">
<head>
<title>{{$details->title}} | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url({{ URL::to('/') }}/photos/<?php echo $details->photo; ?>);">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/knowledge_centre')}}">Knowledge Centre <i class="fa fa-chevron-right"></i></a></span> <span>{{$details->title}} <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">{{$details->title}}</h1>
</div>
</div>
</div>
</section>

<section class="ftco-section">
<div class="container">

<div class="row">

<div class="col-lg-8 ftco-animate blog-single">
<h2 class="mb-3">{{$details->title}}</h2>
<img src="{{ URL::to('/') }}/photos/<?php echo $details->photo; ?>" alt="" class="img-fluid">

<div style="margin-top:2%;">
<?php echo $details->description; ?>
</div>

</div>

@include('sidebar')

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
