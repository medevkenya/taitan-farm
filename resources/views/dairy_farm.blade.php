<!DOCTYPE html>
<html lang="en">
<head>
<title>Dairy Farm | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/dairy/banner.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Dairy Farm <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Dairy Farm</h1>
</div>
</div>
</div>
</section>


<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<h2 class="mb-3">Dairy farm</h2>
<p>
The farm boast of quality, high production and pedigree dairy cows.
We supplies quality, highproduction and pedigree heifers, milkers and calves from; Freshian,
Holsteins, Ayrshire, Fleckvie and Jersey.
We have a reputation to advise and deliver quality dairy animals across the country with production
ranges of between 17 litres per day to 32 litres per day; The farm holds dairy in two locations in Kenya,
Kiambu and at the farm HQ in Taita Taveta
</p>
<ul>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Get a quote of quality dairy animal" target="_blank">
			Get a quote of quality dairy animal
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Get advise on dairy management and dairy farm set up" target="_blank">
		Get advise on dairy management and dairy farm set up
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Get dairy inputs like fodder and silage delivered to you" target="_blank">
			Get dairy inputs like fodder and silage delivered to you
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Request for dairy training " target="_blank">
			Request for dairy training
		</a>
	</li>
</ul>

<h3>Request for quotation</h3>

{!! Form::open(['url' => 'request_dairy_farm_quotation']) !!}
<div class="row">
<div class="col-md-4 form-group">
<label>Your Name</label>
<input class="form-control" type="text" name="name" value="{{old('name')}}" required>
@if ($errors->has('name'))
   <span class="text-danger">{{ $errors->first('name') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Your Mobile No.</label>
<input class="form-control" type="text" name="mobileNo" value="{{old('mobileNo')}}" required>
@if ($errors->has('mobileNo'))
   <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Your Location</label>
<input class="form-control" type="text" name="location" value="{{old('location')}}" required>
@if ($errors->has('location'))
   <span class="text-danger">{{ $errors->first('location') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
<label>Indicate types of dairy animals</label>
<textarea class="form-control" name="description" required>{{old('description')}}</textarea>
@if ($errors->has('description'))
   <span class="text-danger">{{ $errors->first('description') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
	<button type="submit" class="btn btn-primary">Send Request</button>
</div>
</div>
</form>



</div>

@include('sidebar')

<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:10%;">
<div class="container">
<div class="row">
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0020.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/banner.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0029.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0030.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0031.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0033.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0034.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0035.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0040.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0045.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG-20230429-WA0053.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/dairy/IMG_1677.jpg')}});">
</div>
</div>
</div>
</div>
</div>
</section>

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
