<!DOCTYPE html>
<html lang="en">
<head>
<title>Help me start my farm | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/Startyourfarm/banner.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Help me start my farm <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Help me start my farm</h1>
</div>
</div>
</div>
</section>


<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<!-- <h2 class="mb-3">Help me start my farm</h2> -->
<p>Are looking to expand or set up a mixed farm like ours. Get expert advice, supply of inputs, knowledge, project proposals, viability assessment, farm plan management, market survey, agronomy support, breed and seedlings supply, farm construction, farm equipment supply and financing opportunities by filling the form below:</p>

<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:16%;">
<div class="container">
<div class="row">
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/Startyourfarm/IMG-20230429-WA0027.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/Startyourfarm/IMG-20230429-WA0036.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/Startyourfarm/IMG-20230429-WA0137.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/Startyourfarm/IMG-20230429-WA0016.jpg')}});">
</div>
</div>
</div>
</div>
</div>
</section>

<h3>Engage our farm experts</h3>

{!! Form::open(['url' => 'request_start_my_farm_quotation']) !!}
<div class="row">
<div class="col-md-6 form-group">
<label>Your Name</label>
<input class="form-control" type="text" name="name" value="{{old('name')}}" required>
@if ($errors->has('name'))
   <span class="text-danger">{{ $errors->first('name') }}</span>
@endif
</div>
<div class="col-md-6 form-group">
<label>Your Mobile No.</label>
<input class="form-control" type="text" name="mobileNo" value="{{old('mobileNo')}}" required>
@if ($errors->has('mobileNo'))
   <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
@endif
</div>
<div class="col-md-6 form-group">
<label>Your Email</label>
<input class="form-control" type="text" name="email" value="{{old('email')}}" required>
@if ($errors->has('email'))
   <span class="text-danger">{{ $errors->first('email') }}</span>
@endif
</div>
<div class="col-md-6 form-group">
<label>Location of the farm</label>
<input class="form-control" type="text" name="location" value="{{old('location')}}" required>
@if ($errors->has('location'))
   <span class="text-danger">{{ $errors->first('location') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
<label>What ideas do you have on the farm type</label>
<textarea class="form-control" name="description" required>{{old('description')}}</textarea>
@if ($errors->has('description'))
   <span class="text-danger">{{ $errors->first('description') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
	<button type="submit" class="btn btn-primary">Send Request</button>
</div>
</div>
</form>

<p>You can also whatsapp us on <a href="https://api.whatsapp.com/send?phone=<?php echo env("WHATSAPP"); ?>" target="_blank"><span class="fa fa-whatsapp"></span> <?php echo env("WHATSAPP"); ?></a></p>

</div>

@include('sidebar')

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
