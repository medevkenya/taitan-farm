<!DOCTYPE html>
<html lang="en">
<head>
<title>Products & services | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/AgroProcessing/Whitegold/119916135_139650514427219_8444719820129637985_n.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Products & services <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Products & services</h1>
</div>
</div>
</div>
</section>

<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<p>We believe a sustainable agriculture ends at the farmers factory, value added before getting to the market. </p>

<h3>White Gold Yoghurt and Milk</h3>
<p>The farm has a 1,000 Liter per day capacity milk processing cottage industry that produces white gold yoghurt product and sour milk. The factory purchases milk from farmers.
	Currently the factory is temporarily closed but will be re-opened in 7 months’ time. </p>

	<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:16%;">
	<div class="container">
	<div class="row">
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Whitegold/IMG-20230429-WA0127.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Whitegold/factory.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Whitegold/IMG-20230429-WA0131.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Whitegold/IMG-20230429-WA0128.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Whitegold/IMG-20230429-WA0134.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Whitegold/123079814_152566233135647_4334943427183474381_n.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Whitegold/123924447_156611259397811_4889171581031697816_n.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Whitegold/IMG-20230429-WA0132.jpg')}});">
	</div>
	</div>
	</div>
	</div>
	</div>
	</section>

<h3>Avocado value chain</h3>
<p>The farm Value adds Hass Avocado from farmers within our portfolio of the process for export market and production of avocado oil</p>

	<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:16%;">
	<div class="container">
	<div class="row">
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Avocadoexport/IMG-20230429-WA0104.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Avocadoexport/IMG-20230429-WA0074.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Avocadoexport/IMG-20230429-WA0018.jpg')}});">
	</div>
	</div>
	</div>
	<div class="col-md-3 d-flex align-self-stretch ftco-animate">
	<div class="services">
	<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Avocadoexport/IMG-20230429-WA0070.jpg')}});">
	</div>
	</div>
	</div>
	</div>
	</div>
	</section>

	<h3>Beekeeping</h3>
	<p>The farm sets up apiary, supply quality beehives and purchases honey for packaging</p>

		<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:16%;">
		<div class="container">
		<div class="row">
		<div class="col-md-3 d-flex align-self-stretch ftco-animate">
		<div class="services">
		<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Honey/IMG-20230429-WA0061.jpg')}});">
		</div>
		</div>
		</div>
		<div class="col-md-3 d-flex align-self-stretch ftco-animate">
		<div class="services">
		<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Honey/IMG-20230429-WA0081.jpg')}});">
		</div>
		</div>
		</div>
		<div class="col-md-3 d-flex align-self-stretch ftco-animate">
		<div class="services">
		<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Honey/IMG-20230429-WA0099.jpg')}});">
		</div>
		</div>
		</div>
		<div class="col-md-3 d-flex align-self-stretch ftco-animate">
		<div class="services">
		<div class="img" style="background-image: url({{asset('gallery/AgroProcessing/Honey/IMG-20230429-WA0135.jpg')}});">
		</div>
		</div>
		</div>
		</div>
		</div>
		</section>

		<h3>Other services</h3>

		@include('offerred')

</div>

@include('sidebar')

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
