<!DOCTYPE html>
<html lang="en">
<head>
<title>Avocado Farming | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/mainpage/avocado.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Avocado Farming <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Avocado Farming</h1>
</div>
</div>
</div>
</section>


<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<h2 class="mb-3">Taitan Avocado project</h2>
<p>
Hass Avocado has a high fat content that other avocados and do well in tropical climates in Kenya.
We have the market, the knowledge and we have all you need to set up the project successfully.
We are champions of Hass Avocado farming across Kenya and provide end to end services to ensure that you have a successful venture.
<a href="{{URL::to('/knowledge_centre')}}">Learn more on our knowledge center about Hass avocado.</a>
</p>
<p>
We believe on specializing on what we have steady market view on, and what we can confidently sell. Under Avocado projects we provide the following;
</p>
<ul>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Quality Avocado seedlings delivered across the country" target="_blank">
			Quality Avocado seedlings delivered across the country
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Soil tests and agronomist soil correction plan" target="_blank">
		Soil tests and agronomist soil correction plan
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Project viability analysis and estimation of the project returns" target="_blank">
			Project viability analysis and estimation of the project returns
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Guidance in planting, agronomy support and water plan creation" target="_blank">
			Guidance in planting, agronomy support and water plan creation
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Farm management and training on avocado best practices" target="_blank">
			Farm management and training on avocado best practices
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Regular farm visits to check on farm progress" target="_blank">
			Regular farm visits to check on farm progress
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Organic supplies of pesticides and farm inputs required for successful farm orchards" target="_blank">
			Organic supplies of pesticides and farm inputs required for successful farm orchards
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=Market linkages and purchase of avocado for export" target="_blank">
			Market linkages and purchase of avocado for export
		</a>
	</li>
	<li>
		<a href="https://web.whatsapp.com/send?autoload=1&app_absent=0&phone=<?php echo env('WHATSAPP'); ?>&text=We sell avocado oil" target="_blank">
			We sell avocado oil
		</a>
	</li>
</ul>

<p><a href="{{URL::to('/taitan_nurseries')}}" class="btn btn-secondary">Click here to view our nurseries</a></p>

<h3>Request for quotation</h3>


{!! Form::open(['url' => 'request_avocado_farming_quotation']) !!}
<div class="row">
<div class="col-md-4 form-group">
<label>Your Name</label>
<input class="form-control" type="text" name="name" value="{{old('name')}}" required>
@if ($errors->has('name'))
   <span class="text-danger">{{ $errors->first('name') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Your Mobile No.</label>
<input class="form-control" type="text" name="mobileNo" value="{{old('mobileNo')}}" required>
@if ($errors->has('mobileNo'))
   <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
@endif
</div>
<div class="col-md-4 form-group">
<label>Your Location</label>
<input class="form-control" type="text" name="location" value="{{old('location')}}" required>
@if ($errors->has('location'))
   <span class="text-danger">{{ $errors->first('location') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
<label>Describe your request</label>
<textarea class="form-control" name="description" required>{{old('description')}}</textarea>
@if ($errors->has('description'))
   <span class="text-danger">{{ $errors->first('description') }}</span>
@endif
</div>
<div class="col-md-12 form-group">
	<button type="submit" class="btn btn-primary">Send Request</button>
</div>
</div>
</form>



</div>

@include('sidebar')

<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:10%;">
<div class="container">
<div class="row">
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/315299428_583908007069908_5082345555152776954_n.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/IMG-20230429-WA0054.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/IMG-20230429-WA0086.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/IMG-20230429-WA0019.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/IMG-20230429-WA0014.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/316076293_594708179323224_4814892813045065045_n.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/316274835_596417845818924_5546426389132935061_n.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/IMG-20230429-WA0050.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/IMG-20230429-WA0013.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/IMG-20230429-WA0048.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/efdcfda3-85c2-470d-9fa5-08d6dc62ed1c.jpg')}});">
</div>
</div>
</div>
<div class="col-md-3 d-flex align-self-stretch ftco-animate">
<div class="services">
<div class="img" style="background-image: url({{asset('gallery/avocado/IMG-20230429-WA0010.jpg')}});">
</div>
</div>
</div>
</div>
</div>
</section>

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
