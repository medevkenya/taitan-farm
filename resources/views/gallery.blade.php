<!DOCTYPE html>
<html lang="en">
<head>
<title>Gallery | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Our Gallery<i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Our Gallery</h1>
</div>
</div>
</div>
</section>

<section class="ftco-section">
<div class="container">
<div class="row">
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image:url(images/xwork-1.jpg.pagespeed.ic.SFDcY5WYlw.jpg)">
<div class="text">
<h3><a href="{{URL::to('/gallery')}}">Pig Farm</a></h3>
<a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/flavors.jpg);">
<div class="text">
<h3><a href="{{URL::to('/vibecrips')}}">Taitan Flavors</a></h3>
<a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/work-3.jpg);">
<div class="text">
<h3><a href="{{URL::to('/dairyfarm')}}">Farm System</a></h3>
<a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/work-4.jpg);">
<div class="text">
<h3><a href="{{URL::to('/pigfarm')}}">Fresh Pock</a></h3>
<a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/work-5.jpg);">
<div class="text">
<h3><a href="{{URL::to('/eggs')}}">Poultry Farm</a></h3>
<a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/services-1.jpg);">
<div class="text">
<h3><a href="{{URL::to('/vibecrips')}}">Vibe Crips</a></h3>
<a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/work-7.jpg);">
<div class="text">
<h3><a href="{{URL::to('/dairyproducts')}}">Havesting Innovation</a></h3>
<a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
<div class="col-md-3 ftco-animate">
<div class="project-wrap img d-flex align-items-end" style="background-image: url(images/bull.jpg);">
<div class="text">
<h3><a href="{{URL::to('/dairyfarm')}}">Bull Fattening</a></h3>
<a href="portfolio-single.html" class="icon d-flex align-items-center justify-content-center"><span class="fa fa-chevron-right"></span></a>
</div>
</div>
</div>
</div>
<div class="row mt-5">
<div class="col text-center">
<div class="block-27">
<ul>
<li><a href="#">&lt;</a></li>
<li class="active"><span>1</span></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#">5</a></li>
<li><a href="#">&gt;</a></li>
</ul>
</div>
</div>
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
