<!DOCTYPE html>
<html lang="en">
<head>
<title>Home | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Agro Express <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Agro Express</h1>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-no-pt ftco-no-pb ftco-about img">
<div class="container">
<div class="row d-flex">
<div class="col-md-12 about-intro">
<div class="row d-flex">
<div class="col-md-6 d-flex align-items-stretch">
<div class="img d-flex align-items-center align-self-stretch justify-content-center" style="background-image:url(images/BG_2.jpg)">
<!--<div class="year-stablish text-center">
<div class="icon2"><span class="flaticon-calendar"></span></div>
<div class="text">
<strong class="number" data-number="05">0</strong>
<span>Year Of<br> Experience</span>
</div>
</div>-->
<div class="img-2 d-flex align-items-center justify-content-center" style="background-image:url(images/about-2.jpg)">
</div>
</div>
</div>
<div class="col-md-6 pl-md-5 py-5">
<div class="row justify-content-start pb-3">
<div class="col-md-12 heading-section ftco-animate">
<span class="subheading">More than farming</span>
<h2 class="mb-4">Supporting other small scall farmers</h2>
<p>This is a wing of the far that deal with Agribusiness supplies to government and nongovernmental institutions. Taitan Farm has undertaken a number of Agribusiness LPO's from
Taita Taveta county and Kenya Climate Smart Agriculture.</p>
<p>Investment in agricalture is the best weapon against poverty and hunger, and
   they have made better life for billions of people. We exist to serve the
   community through creation of a reliable market with high quality farm products
    that create satisfaction to both consumers and small scale farmers</p>
<!--<div class="row my-4">
<div class="col-md-6 ftco-animate">
<div class="services-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-agriculture"></span></div>
<div class="media-body">
<h3 class="heading">Growing Fruits<br> and Vegetables</h3>
</div>
</div>
</div>
<div class="col-md-6 ftco-animate">
<div class="services-2 d-flex align-items-center">
<div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-agriculture-2"></span></div>
<div class="media-body">
<h3 class="heading">Tips for Ripening<br> Fruits</h3>
</div>
</div>
</div>
</div>-->
</div>
<!--<p><a href="#" class="btn btn-secondary">Learn More</a></p>-->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
