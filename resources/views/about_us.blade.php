<!DOCTYPE html>
<html lang="en">
<head>
<title>About us | <?php echo env('APP_NAME'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('gallery/AgroProcessing/Whitegold/119916135_139650514427219_8444719820129637985_n.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>About us <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">About us</h1>
</div>
</div>
</div>
</section>

<section class="ftco-section-pages">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">

	@if (count($errors) > 0)
   <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
  @endif

  @if ($message = Session::get('error'))
       <div class="alert alert-danger">
           {{ $message }}
       </div>
  @endif

  @if ($message = Session::get('success'))
       <div class="alert alert-success">
           {{ $message }}
       </div>
  @endif

<p>Registered in 2018 Taitan Farm Limited is the largest youth led mixed agriculture one stop shop in coastal Kenya, Taita Taveta, Wundanyi Town. We have 60 percent women and 80% youths among our 41 workforce and in leadership. We have driven strong partnership with private organizations, associations, non-governmental and governmental organizations to ensure that we impact the community first as we meet our investment returns goals. We seek to redefine agriculture through innovation, agro-processing, value chain expansion and quality services. We only what we have visibility of a steady market and we are sure of good returns. We drive our pride by demonstrating to youths how agribusiness can transform Africa, creating employment and offering farmer support and opportunities. We have created a community of over 1,400 that we provide direct mentorship on.</p>

<p>
We provide end to end expert support in Farm viability assessment, Agroprocessing, Avocado value chain, Dairy management, Bull fattening, water management, German shepherd dogs, Pig farming, poultry, soil testing and agronomy support, farm management, farm input services, farm set up and agribusiness opportunities. We have also partnered with other farms to offer what is beneficially to farmers but we have no specialty in. We can reach any part of Kenya and East Africa and can offer quick services through our satellite branches in Central Kenya, Kajiado, Loitoktok and Taita Taveta where the head office is.
</p>

		<h3>Other services</h3>

    @include('offerred')

    <h3>Our mission</h3>
    <p>Redefining agribusiness</p>

    <h3>Our slogan</h3>
    <p>Chemichemi ya kilimo biashara</p>

		<h3>Contact us</h3>
    <p>Fill in your request</p>

		{!! Form::open(['url' => 'request_contact']) !!}
		<div class="row">
		<div class="col-md-8 form-group">
		<label>Your Name</label>
		<input class="form-control" type="text" name="name" value="{{old('name')}}" required>
		@if ($errors->has('name'))
		   <span class="text-danger">{{ $errors->first('name') }}</span>
		@endif
		</div>
		<div class="col-md-4 form-group">
		<label>Your Mobile No.</label>
		<input class="form-control" type="text" name="mobileNo" value="{{old('mobileNo')}}" required>
		@if ($errors->has('mobileNo'))
		   <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
		@endif
		</div>
		<div class="col-md-12 form-group">
		<label>Your comment</label>
		<textarea class="form-control" name="description" required>{{old('description')}}</textarea>
		@if ($errors->has('description'))
		   <span class="text-danger">{{ $errors->first('description') }}</span>
		@endif
		</div>
		<div class="col-md-12 form-group">
			<button type="submit" class="btn btn-primary">Send Request</button>
		</div>
		</div>
		</form>

		<p>Whatsapp link: <a href="https://wa.me/message/53MIHYMB2PERO1" target="_blank">https://wa.me/message/53MIHYMB2PERO1</a></p>

		<p>Email: <a href="mailto:info@taitanfarm.com">info@taitanfarm.com</a> OR <a href="mailto:taitanfarm@gmail.com">taitanfarm@gmail.com</a></p>

		<p>Avocado project Email: <a href="mailto:avocado@taitanfarm.com">avocado@taitanfarm.com</a> OR <a href="mailto:taitanfarm@gmail.com">taitanfarm@gmail.com</a></p>

		<p>
		Whatsapp or Call: +254735777770<br>
		Head Office<br>
		Taitan Farm HQ, Werugha, Wundanyi, Taita, Coastal Kenya<br>
		P.O Box 31 – 80300<br>
		Taita Taveta<br>
		Kenya
		</p>

		<p>Scan the QR Codes below to connect with us on social media</p>

		<section class="ftco-section ftco-services ftco-no-pt" style="margin-top:16%;">
		<div class="container">
		<div class="row">
		<div class="col-md-4 d-flex align-self-stretch ftco-animate">
		<div class="services">
		<div class="img" style="background-image: url({{asset('gallery/QRcodes/WhatsApp.jpg')}});">
		</div>
		</div>
		</div>
		<div class="col-md-4 d-flex align-self-stretch ftco-animate">
		<div class="services">
		<div class="img" style="background-image: url({{asset('gallery/QRcodes/Instagram.jpg')}});">
		</div>
		</div>
		</div>
		<div class="col-md-4 d-flex align-self-stretch ftco-animate">
		<div class="services">
		<div class="img" style="background-image: url({{asset('gallery/QRcodes/Tiktok.jpg')}});">
		</div>
		</div>
		</div>
		</div>
		</div>
		</section>


</div>

@include('sidebar')

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
