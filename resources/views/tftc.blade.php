<!DOCTYPE html>
<html lang="en">
<head>
<title>Taitan Farm Training  Centre | Taitan Farm</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@include('headerlinks')
</head>
<body>

@include('header')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/about-1.jpg');">
<div class="overlay"></div>
<div class="overlay-2"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
<div class="col-md-9 ftco-animate pb-5 text-center">
<p class="breadcrumbs"><span class="mr-2"><a href="{{URL::to('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Taitan Farm Training  Centre <i class="fa fa-chevron-right"></i></span></p>
<h1 class="mb-0 bread">Taitan Farm Training  Centre</h1>
</div>
</div>
</div>
</section>

<section class="ftco-section">
<div class="container">
<div class="row">
<div class="col-lg-8 ftco-animate blog-single">
<h2 class="mb-3">Taitan Farm Training  Centre</h2>
<p>The farm has partnered with many organizations to offer training
and capacity building in agriculture to farmer, youths and women. The farm organizes an
annual 'TAITAN FARM GOAT EATING' summit that brings together farmers, agriprenuers,
youths and women to get knowledge on agribusiness from different experts.</p>
<p>
<img src="{{asset('images/tftc.PNG')}}" alt="" class="img-fluid">
</p>
<p><a href="{{asset('images/TaitanFarmTrainigCentreprofile.pdf')}}" target="_blank" class="btn btn-secondary">Click to download resume</a></p>
</div>

@include('sidebar')

</div>
</div>
</section>

@include('newsletter')

@include('footer')

@include('footerlinks')

</body>
</html>
