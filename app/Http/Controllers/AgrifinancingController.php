<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Agrifinancing;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;

class AgrifinancingController extends Controller
{

  public function agri_financing(Request $request) {
    $list = Agrifinancing::getAll();
    return view('agri_financing',['list'=>$list]);
  }

public function addknowledge(Request $request)
{

request()->validate([
    'description' => 'required',
    'title' => 'required|string|min:3',
    ], [
        'title.required' => 'Title is required',
    ]);

  $check = Locations::where('title', request()->title)->where('isDeleted', 0)->first();
  if ($check) {
      return Redirect::back()->with('error','Title already exists.');
  } else {
      $add = Locations::storeone(request()->all());
      if ($add) {
          return Redirect::back()->with('success','New item was created successfully.');
      } else {
          return Redirect::back()->with('error','Error occurred while creating knowledge.');
      }
  }
}

public function editknowledge(Request $request)
{

request()->validate([
    'description' => 'required',
    'title' => [
        'required',
        Rule::unique('knowledge_centre')->ignore($request->id),
    ],
    ], [
        'title.required' => 'Title is required',
    ]);

      $update = Locations::updateone(request()->all());
      if ($update) {
          return Redirect::back()->with('success','The item was updated successfully');
      } else {
          return Redirect::back()->with('error','Error occurred while updating knowledge');
      }
}

public function deleteknowledge(Request $request)
{

  $delete = Locations::deleteone(request()->id);
  if ($delete) {
      return Redirect::back()->with('success','Item was deleted successfully');
  } else {
      return Redirect::back()->with('error','Error occurred while deleting knowledge');
  }
}

}
