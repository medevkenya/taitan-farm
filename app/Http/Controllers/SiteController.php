<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\User;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;//
use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;

class SiteController extends Controller
{

  public function about()
  {
      return view('about');
  }

}
