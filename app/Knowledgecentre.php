<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use Mail;
use Str;

class Knowledgecentre extends Model
{

    protected $table = 'knowledge_centre';

    public static function getAll()
    {
      return Knowledgecentre::where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function getDetails($slug) {
      return Knowledgecentre::where('slug',$slug)->where('isDeleted',0)->first();
    }

  public static function storeone($all)
  {

      $model = new Knowledgecentre;
      $model->title = $all['title'];
      $model->description = $all['description'];
      $model->slug = self::generateSlug($all['title']);
      $model->save();
      if ($model)
      {
        return true;
      }

      return false;

  }

  public static function updateone($all)
  {

      $model = Knowledgecentre::find($all['id']);
      $model->title = $all['title'];
      $model->description = $all['description'];
      $model->slug = self::generateSlug($all['title']);
      $model->save();
      if ($model) {
          return true;
      }
      return false;

  }

  public static function deleteone($id)
  {
      $model = Knowledgecentre::find($id);
      $model->isDeleted = 1;
      $model->save();
      if ($model) {
        return true;
      }
      return false;
  }

  public function generateSlug($name)
  {   
    //https://www.positronx.io/laravel-generate-multi-unique-slug-on-page-load-tutorial/
    if (Knowledgecentre::whereSlug($slug = Str::slug($name))->exists()) {
        $max = Knowledgecentre::wheretitle($name)->latest('id')->skip(1)->value('slug');
        if (isset($max[-1]) && is_numeric($max[-1])) {
            return preg_replace_callback('/(\d+)$/', function($mathces) {
                return $mathces[1] + 1;
            }, $max);
        }
        return "{$slug}-2";
    }
    return $slug;
  }

}
