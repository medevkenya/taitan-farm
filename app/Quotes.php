<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use Mail;

class Quotes extends Model
{

    protected $table = 'quotes';

    public static function getAll()
    {
          return Quotes::where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function saveNew($firstName,$lastName,$mobileNo,$service,$message) {
      $model = new Quotes;
      $model->firstName = $firstName;
      $model->lastName = $lastName;
      $model->mobileNo = $mobileNo;
      $model->service = $service;
      $model->message = $message;
      $model->save();
      if($model) {

        $datanot=array('name'=>$firstName." ".$lastName,'mobileNo'=>$mobileNo,'service'=>$service,'messagecontent'=>$message);

           Mail::send('emails.quote', $datanot, function($message) use ($email) {
           $message->to(env("HELPEMAIL"))->subject('Request for Quote');
           $message->from(env("MAIL_FROM_ADDRESS"),env("MAIL_FROM_NAME"));
           });

        return true;
      }
      else {
        return false;
      }
    }

}
