<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class EmailSubscriptions extends Model
{

    protected $table = 'email_subscriptions';

    public static function getAll()
    {
          return EmailSubscriptions::where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function saveNew($email) {
      $model = new EmailSubscriptions;
      $model->email = $email;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

}
