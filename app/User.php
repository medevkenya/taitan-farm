<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Hash;
use Mail;
use App\Responseobject;
use App\Roles;
use App\Userpermissions;
use App\Subscriptions;
use Response;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName','lastName','mobileNo','companyId','userTypeId','isVerified','isDisabled','adminId','about','location', 'email', 'password','profilePic'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function checkAccess($hashedKey) {

      return true;

    }

    public static function getUserBymobileNo($mobileNo) {
      $appurl = User::getMainURL();
        $udata = User::select('id','mobileNo','email','firstName','lastName','userTypeId','isOnline','dob','about','wardId','location','profilePic','isActive')
        ->where('mobileNo',$mobileNo)->where('isDeleted',0)->first();
        if($udata) {
        return array('id'=>$udata->id,'firstName'=>$udata->firstName,'lastName'=>$udata->lastName,'mobileNo'=>$udata->mobileNo,'email'=>$udata->email,'userTypeId'=>$udata->userTypeId,'isOnline'=>$udata->isOnline);
      }
      else {
        return false;
      }
    }

    public static function getUserByEmail($email) {
      $appurl = User::getMainURL();
        $udata = User::select('id','mobileNo','email','firstName','lastName','userTypeId','isOnline','dob','about','wardId','location','profilePic','isActive')
        ->where('email',$email)->where('isDeleted',0)->first();
        if($udata) {
        $profilePic = User::getMainURL()."images/".$udata->profilePic;
        return array('id'=>$udata->id,'profilePic'=>$profilePic,'firstName'=>$udata->firstName,'lastName'=>$udata->lastName,'mobileNo'=>$udata->mobileNo,'email'=>$udata->email,'userTypeId'=>$udata->userTypeId,'isOnline'=>$udata->isOnline);
      }
      else {
        return false;
      }
    }

    public static function getUserProfileById($id) {
  $appurl = User::getMainURL();
    $udata = User::select('id','mobileNo','email','firstName','lastName','userTypeId','isOnline','isVerified')
    ->where('id',$id)->where('isDeleted',0)->first();

    return array('id'=>$udata->id,'isVerified'=>$udata->isVerified,'firstName'=>$udata->firstName,'lastName'=>$udata->lastName,'mobileNo'=>$udata->mobileNo,'email'=>$udata->email,'userTypeId'=>$udata->userType,'isOnline'=>$udata->isOnline);

}

    public static function getUserById($id) {
        return User::select('id','mobileNo','email','firstName','lastName','userTypeId','isOnline','dob','about','wardId','location','profilePic','isActive')
        ->where('id',$id)->where('isDeleted',0)->first();
    }

    public static function loginUser($email,$password)
    {
        //$mobileNo = "254".substr($mobileNo, -9);
		    //$userDetails = User::where('mobileNo',$mobileNo)->first();

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
			//if (Hash::check($pass, $userDetails->password)) {

            //make user active
            User::where('email', $email)->update(['isOnline' => 1]);

            return true;
        } else {
            return false;
        }
    }

    public static function registerUser($all) {

      $model = new User;
      $model->firstName = $all['firstName'];
      $model->lastName = $all['lastName'];
      $model->mobileNo = "254".substr($all['mobileNo'], -9);
      $model->email = $all['email'];
      $model->password = Hash::make($all['password']);
      $model->save();
      if($model) {

        $email = $all['email'];

        $id = $model->id;

        $code=md5(date('Y-m-d: H:i:sa').$id);
        User::where(['id'=>$id])->update(['resetCode'=>$code,'adminId'=>$id]);

        ///////////////////////////////
        $modelr = new Roles;
        $modelr->roleName = "Admin";
        $modelr->adminId = $id;
        $modelr->created_by = $id;
        $modelr->save();
        Userpermissions::seedPermissions($modelr->id);

        sleep(2);

        Subscriptions::trialSubscription($id);
        /////////////////////////////

         $link=url('/activateAccount?c='.$code.'&k='.$id);

         $datanot=array('name'=>$all['firstName']." ".$all['lastName'],'email'=>$all['email'],'link'=>$link);

          // Mail::send('emails.regtemp', $datanot, function($message) use ($email) {
          // $message->to($email)->subject('Activate Account');
          // $message->from('mails@globaltempingservices.com','Global Temping Services');
          // });

        return true;

      }
      else {
        return false;
      }
    }

    public static function resetPassword($email) {

    $userdetails = User::where('email',$email)->first();
    $pass=str_random(32);
    $name = $userdetails->firstName." ".$userdetails->lastName;

    $data=array('pass'=>$pass,'name'=>$name,'email'=>$email,);

    $edituserdata = User::where('email', $email)->update(['confirmpassword' => $pass]);
    if ($edituserdata) {

      Mail::send('emails.forgotpintemplate', $data, function($message) use ($pass,$email) {
      $message->to($email)->subject('Recover Password');
      $message->from('mails@globaltempingservices.com','Global Temping Services');
      });

    return true;

    }
    else {
      return false;
    }

    }

    public static function forgotpass($email) {
      $ii= strtoupper(str_shuffle(time().$email."abcdefghijklmnopqrstuvwxyz"));
      $password = substr($ii, -6);
      $model = User::where('email', $email)->update(['password' => Hash::make($password)]);
      if ($model) {
        //SMS::sendSMS($mobileNo,"Your new password is ".$password."");
        return true;
      }
      else {
        return false;
      }
    }


    public static function editMyProfile($all) {
      //$response = new Responseobject;
      $id	= Auth::user()->id;
      $model = User::find($id);
      $model->firstName = $all['firstName'];
      $model->lastName = $all['lastName'];
      $model->email = $all['email'];
      $model->mobileNo = "254".substr($all['mobileNo'], -9);
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function editPic($id,$profilePic)
    {
      $response = new Responseobject;
        $model = User::where('id', $id)->update(['profilePic' => $profilePic]);
        if ($model) {
          $profilePic = User::getMainURL()."images/".$profilePic;
                  $response->status = $response::status_ok;
                  $response->code = $response::code_ok;
                  $response->message = "Your profile picture was updated successfully";
                  $response->result = $profilePic;

                } else {
                  $response->status = $response::status_fail;
                  $response->code = $response::code_fail;
                  $response->message = "Failed to update profile picture. Please try again";
                  $response->result = null;
                }
                return Response::json($response);
    }


    public static function editProfile($id,$firstName,$lastName,$mobileNo) {
      $response = new Responseobject;
      $model = User::find($id);
      $model->mobileNo = "254".substr($mobileNo, -9);
      $model->lastName = $lastName;
      $model->firstName = $firstName;
      $model->save();
      if($model) {
        $response->status = $response::status_ok;
        $response->code = $response::code_ok;
        $response->message = "Your profile was updated successfully";
        $response->result = null;
      }
      else {
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = "Failed to update your profile. Please try again";
        $response->result = null;
      }
        return Response::json($response);
    }

    /**
      * Update
      */
      public static function changePassword($userId,$newpassword,$oldpassword)
      {
        $response = new Responseobject;

        $hashedpassword = Hash::make($newpassword);

          $checkdata =User::where('id', $userId)->first();
          if(!Hash::check($oldpassword, $checkdata->password)){
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = "The current password is incorrect";
            $response->result = null;
            }else{
                // write code to update password
                $adddata =User::where('id', $userId)->update(['password' => $hashedpassword]);
                if ($adddata) {
                  $response->status = $response::status_ok;
                  $response->code = $response::code_ok;
                  $response->message = "Your password was changed successfully";
                  $response->result = null;
                } else {
                  $response->status = $response::status_fail;
                  $response->code = $response::code_fail;
                  $response->message = "Failed to change your password. Please try again";
                  $response->result = null;
                }
            }

          return Response::json($response);

      }

      public static function storeone($firstName,$lastName,$email,$password,$roleId)
      {
          $adminId	= Auth::user()->adminId;
          $created_by	= Auth::user()->id;
          $model = new User;
          $model->firstName = $firstName;
          $model->lastName = $lastName;
          $model->password = $password;
          $model->email = $email;
          $model->roleId = $roleId;
          $model->adminId = $adminId;
          $model->created_by = $created_by;
          $model->save();
          if ($model) {
              Activities::saveLog("Added user [".$firstName." ".$lastName." - ".$email."]");
              return true;
          }
          return false;
      }

      public static function updateone($id, $firstName,$lastName,$email,$roleId)
      {
          $model = User::find($id);
          $model->firstName = $firstName;
          $model->lastName = $lastName;
          $model->roleId = $roleId;
          $model->email = $email;
          $model->save();
          if ($model) {
              Activities::saveLog("Edited user [".$firstName." ".$lastName." - ".$email."]");
              return true;
          }
          return false;
      }

      public static function deleteone($id)
      {
          $model = User::find($id);
          $model->isDeleted = 1;
          $model->save();
          if ($model) {
              Activities::saveLog("Deleted user [".$id."]");
              return true;
          }
          return false;
      }

      public static function getAll() {
        return User::select('id','firstName','lastName')->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
      }

    public static function getBaseUrl() {
      return "http://pgms.alternet.co.ke/";
    }

    public static function getMainURL() {
      return "http://pgms.alternet.co.ke/";
    }
}
